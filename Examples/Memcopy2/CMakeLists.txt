IF(CAPI_SIM_LIBCXL_FOUND OR CAPI_SYN_LIBCXL_FOUND)
    ADD_EXECUTABLE(host_memcopy2 host_memcopy2.cpp)
    TARGET_LINK_LIBRARIES(host_memcopy2 BlueLinkHost pthread ${CAPI_CXL_LIBRARY})
ENDIF()

IF(Bluespec_FOUND)
    ADD_BSV_PACKAGE(Memcopy2 AFU PSLTypes AFUShims DedicatedAFU ResourceManager)
    ADD_BLUESPEC_VERILOG_OUTPUT(Memcopy2 mkMemcopy2AFU)
ENDIF()

IF(CAPI_SIM_LIBCXL_FOUND)
    ## Post-build commands for CAPI simulation
    
    #ADD_CUSTOM_COMMAND(TARGET verilog_mkMemcopy2AFU POST_BUILD
    #    COMMAND ${VSIM_VLOG_EXECUTABLE} -work work mkMemcopy2AFU.v
    #    COMMAND ${VSIM_VLOG_EXECUTABLE} -work work +define+MODULENAME=mkMemcopy2AFU +define+HA_ASSIGNMENT_DELAY=${BlueLink_HA_ASSIGNMENT_DELAY} ${CMAKE_SOURCE_DIR}/PSLVerilog/top.v ${CMAKE_SOURCE_DIR}/PSLVerilog/revwrap.v
    #    WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR} 
    #    )
    #
    #
    ### Run CAPI sim
    #
    #ADD_CUSTOM_TARGET(sim_Memcopy2 DEPENDS verilog_mkMemcopy2AFU host_memcopy2)
    #
    #ADD_CUSTOM_COMMAND(TARGET sim_Memcopy2 POST_BUILD
    #    COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_SOURCE_DIR}/pslse.parms   ${CMAKE_CURRENT_BINARY_DIR}
    #    COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_SOURCE_DIR}/shim_host.dat ${CMAKE_CURRENT_BINARY_DIR}
    #    COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_SOURCE_DIR}/pslse_server.dat ${CMAKE_CURRENT_BINARY_DIR}
    #    COMMAND ${VSIM_VSIM_EXECUTABLE} -batch -onfinish exit -logfile transcript -t 1ns -L altera_mf_ver -L bsvlibs -L bsvaltera -L work -do "run -all" -pli ${CAPI_SIM_PLI_DRIVER} mkMemcopy2AFU_pslse_top
    #    )
    ADD_CAPI_SIM(Memcopy2 mkMemcopy2AFU)
ENDIF()
