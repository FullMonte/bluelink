/*
 * host_memcopy.c
 *
 *  Created on: Mar 25, 2015
 *      Author: jcassidy
 */
#include <errno.h>

#include <getopt.h>

#include <linux/types.h>

#include <stdio.h>

#include <stdlib.h>

#include <string.h>

#include <time.h>

#include "libcxl.h"

#include <chrono>


#include <cinttypes>
#include <boost/random/mersenne_twister.hpp>

#include <boost/align/aligned_allocator.hpp>

#include <boost/range.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/adaptor/indexed.hpp>
#include <boost/random/uniform_real.hpp>
#include <boost/random/variate_generator.hpp>

#include <BlueLink/Host/AFU.hpp>
#include <BlueLink/Host/WED.hpp>

#include <iostream>
#include <iomanip>
#include <functional>
#include <fstream>
#include <vector>

#include<time.h>
#include<sys/time.h>

#define DEVICE_STRING "/dev/cxl/afu0.0d"
#define HARDWARE

// call this function to start a nanosecond-resolution timer
struct timespec timer_start(){
    struct timespec start_time;
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &start_time);
//gettimeofday(&start_time, NULL);  
  return start_time;
}

// call this function to end a timer, returning nanoseconds elapsed as a long
long timer_end(struct timespec start_time){
    struct timespec end_time;
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &end_time);
//gettimeofday(&end_time, NULL);    
long diffInNanos = (end_time.tv_sec - start_time.tv_sec) * (long)1e9 + (end_time.tv_nsec - start_time.tv_nsec);
    return diffInNanos;
}


struct MemcopyWED {
	uint64_t	addr_from;
	uint64_t	addr_to;
	uint64_t	size;

	uint64_t	resv[13];
};

#define STATUS_READY 0x1ULL
#define STATUS_WAITING 0x2ULL
#define STATUS_RUNNING 0x3ULL
#define STATUS_DONE 0x4ULL

using namespace std;

int main (int argc, char *argv[])
{
#ifdef HARDWARE
	const bool sim = false;
#else
	const bool sim = true;
#endif

typedef boost::random::mt19937 base_generator_type;
base_generator_type rng_f;
boost::uniform_real<> uni_dist(0,1);
boost::variate_generator<base_generator_type&, boost::uniform_real<> > uni(rng_f, uni_dist);


	const size_t Nbytes = argc > 1 ? atoi(argv[1]) : 65536;//65536; ;//4096;//8192;//4194304;//65536;
	const size_t Ndw=Nbytes/64;

	// allocate space for input/output/golden
//	vector<
//		uint64_t,
//		boost::alignment::aligned_allocator<uint64_t,128>> golden(Ndw), input(Ndw), output(3*Ndw,0);

 vector<std::array<uint64_t,8>,boost::alignment::aligned_allocator<std::array<uint64_t,8>,128>> golden(Ndw), input(Ndw), output(3*Ndw);

	assert(boost::alignment::is_aligned(128,golden.data()));
	assert(boost::alignment::is_aligned(128,input.data()));
	assert(boost::alignment::is_aligned(128,output.data()));


	// generate stimulus
	boost::random::mt19937_64 rng;
	for (unsigned j=0; j<Ndw; j++)
	{
		for (int i=0; i<8; i++) 
 	 	{
			golden[j][i]=uni();
  			
		}
	}

//	boost::generate(golden, std::ref(rng));
	boost::copy(golden, input.begin());
/*
	ofstream os("golden.hex");
	for(const auto w : golden)
		os << setw(16) << hex << w << endl;
	os.close();
*/
	AFU afu(DEVICE_STRING);

	StackWED<MemcopyWED,128,128> wed;

	wed->addr_from=(uint64_t)input.data();
	wed->addr_to=(uint64_t)(output.data()+Ndw);
	wed->size=Nbytes;

	cout << "From: " << hex << setw(16) << wed->addr_from << endl;
	cout << "  To: " << hex << setw(16) << wed->addr_to << endl;
	cout << "Size: " << hex << setw(16) << wed->size << endl;

	//stream start
//        struct timespec afutime = timer_start();


	afu.start(wed.get());

//	long time_elapsed_nanos_afu = timer_end(afutime);
  //      cout << "Time taken to start afu: " << time_elapsed_nanos_afu << endl;
    //    printf(" Time taken (nanoseconds): %ld\n", time_elapsed_nanos_afu);

	unsigned long long st=0;

	//stream start
//         struct timespec MMIOtime = timer_start();

	
	auto timeStream1 = chrono::high_resolution_clock::now();

	unsigned N;
	for(N=0;N<100 && (st=afu.mmio_read64(0)) != STATUS_WAITING;++N)
	{
		cout << "  Waiting for 'waiting' status (st=" << st << " looking for " << STATUS_WAITING << ")" << endl;
		usleep(sim ? 100000 : 100);
	}

	for(unsigned i=0;i<4;++i)
		cout << "MMIO[" << setw(6) << hex << (i<<3) << "] " << setw(16) << hex << afu.mmio_read64(i<<3) << endl;

	 //start
              //  struct timespec donetime = timer_start();


	//auto timeStream1 = chrono::high_resolution_clock::now();

	cout << "Starting" << endl;
	afu.mmio_write64(0,0x0ULL);		// start signal: write 0 to MMIO 0

	 //done
  //      long time_elapsed_nanos_MMIO = timer_end(MMIOtime);
    //    cout << "Time taken to waiting status and MMIO start: : " << time_elapsed_nanos_MMIO << endl;
      //  printf(" Time taken (nanoseconds): %ld\n", time_elapsed_nanos_MMIO);


	unsigned timeout=1000;

	for(N=0;N < timeout && (st=afu.mmio_read64(0)) != STATUS_DONE;++N)	// wait for done status
	{
		cout << "  status " << st << endl << flush;
		usleep(sim ? 100000 : 1000);
	}


	if (N == timeout)
		cout << "ERROR: Timeout waiting for done status" << endl;


	 //done
       //  long time_elapsed_nanos_done = timer_end(donetime);
      //  cout << "Time taken to finish transfer: : " << time_elapsed_nanos_done << endl;
      //  printf(" Time taken (nanoseconds): %ld\n", time_elapsed_nanos_done);


	auto timeStream2 = chrono::high_resolution_clock::now();
        printf("chrono TOTAL::duration in ns : %ld \n", chrono::duration_cast<chrono::nanoseconds>(timeStream2 - timeStream1).count() );
	

	cout << "Terminating" << endl;
	afu.mmio_write64(0,0x1ULL);
/*
	  //done
         long time_elapsed_nanos_done = timer_end(donetime);
        cout << "Time taken to finish transfer: : " << time_elapsed_nanos_done << endl;
        printf(" Time taken (nanoseconds): %ld\n", time_elapsed_nanos_done);
*/


	bool ok=true;

	// check for correct copy and no corruption
/*	for(size_t i=0;i<Ndw;++i)
		if (golden[i] != input[i])
		{
			ok = false;
			cerr << "Corrupted input data at " << setw(16) << hex << i << endl;
		}

	size_t i;

	for(i=0; i<Ndw; ++i)
		if (output[i] != 0)
		{
			ok = false;
			cerr << "Corrupted output data at output offset " << setw(16) << i << hex << endl;
		}
*/
size_t i= Ndw;

	for(;i<2*Ndw;++i)
                if (output[i] != golden[i-Ndw])
                {
                ok = false;
//                cerr << "Mismatch at " << setw(16) << hex << i-Ndw << " expecting " << setw(16) << golden[i-Ndw] << " got " << setw(16) << output[i] << endl;
                }
	

/*
	for(;i<3*Ndw;++i)
		if (output[i] != 0)
		{
			ok = false;
			cerr << "Corrupted output data at output offset " << setw(16) << hex << i << endl;
		}

	// write output data to file
	os.open("output.hex");
	for(size_t i=Ndw; i<2*Ndw; ++i)
		os << setw(16) << hex << output[i] << endl;
*/
	if (ok)
		cout << "Checks passed!" << endl;

	return ok ? 0 : -1;
}
