/*
 * host_memcopy.c
 *
 *  Created on: Mar 25, 2015
 *      Author: jcassidy
 */

#include <cinttypes>
#include <boost/random/mersenne_twister.hpp>

#include <boost/align/aligned_allocator.hpp>

#include <boost/range.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/adaptor/indexed.hpp>
#include <boost/random/uniform_real.hpp>
#include <boost/random/variate_generator.hpp>

#include <BlueLink/Host/AFU.hpp>
#include <BlueLink/Host/WED.hpp>

#include <iostream>
#include <iomanip>
#include <functional>
#include <fstream>
#include <vector>
#include <math.h>
#define DEVICE_STRING "/dev/cxl/afu0.0d"

struct MemcopyWED {
	uint64_t	addr_from0; // start address of array to copy from. 2 addresses for double buffering. In case of block transfer, only addrFrom0 is used
	uint64_t	addr_from1;
	uint64_t	addr_to; // start address of array to copy to
	uint64_t	size; // number of bytes to copy in each iteration
	uint64_t        buffered; // 1 if we are using double-buffering, 0 if we are doing block transfer

	uint64_t	resv[11];
};

#define STATUS_READY 0x1ULL
#define STATUS_WAITING 0x2ULL
#define STATUS_RUNNING 0x3ULL
#define STATUS_DONE 0x4ULL

//#define DOUBLE_BUFFERED
#define HARDWARE
#define DEFAULT
#define PI 3.14159265
typedef boost::random::mt19937 base_generator_type;

using namespace std;

int main (int argc, char *argv[])
{
#ifdef HARDWARE
	const bool sim = false;
#else
	const bool sim = true;
#endif
//	const size_t Nbytes=65536;
const size_t Nbytes= 1024;
	//const size_t Nbytes=32768;
    //const size_t Nbytes=16384;
	//const size_t NbytesPerCopy=65536;
	const size_t NbytesPerCopy=512;
    //const size_t NbytesPerCopy=4096;
	//const size_t Ndw=Nbytes/16; //struct with two uint64_t
       // const size_t Ndw= Nbytes/4;//32 bit float
        const size_t Ndw= Nbytes/64; //2 float in a structure
	const size_t bufferSize=NbytesPerCopy/8;
//struct alignas(128) number
 //{
//uint64_t first;
//uint64_t second;
//};

//typedef float number;
struct number
  { uint64_t first;
    uint64_t  second;
uint64_t dummy0=0;
uint64_t dummy1=0;

uint64_t dummy2=0;
uint64_t dummy3=0;


uint64_t dummy4=0;
uint64_t dummy5=0;


  };
 
base_generator_type rng_f;
boost::uniform_real<> uni_dist(0,1);
boost::variate_generator<base_generator_type&, boost::uniform_real<> > uni(rng_f, uni_dist);


float rand_pi; 
//float rand_z;
float num;
float dirx, diry, dirz;
float dirz_sqrt;

struct packet 
{
float dir[3];
uint32_t tetID= 1234;
float pos[3]={0};
};



vector<packet> launch(10);

vector<number,boost::alignment::aligned_allocator<number,128>> golden(Ndw), input(Ndw), output(3*Ndw);

for (int i=0; i<10; i++)

{
    //cout <<"random number is :" << uni() << endl;
    rand_pi= uni()*PI*2;
    dirz= uni();
    dirz_sqrt= pow((1-dirz*dirz),0.5);
    dirx = dirz_sqrt* cos(rand_pi);
    diry = dirz_sqrt* sin(rand_pi);
   
    //
    //
    
    

    //
    launch[i].dir[0]= dirx;
    launch[i].dir[1]= diry;
    launch[i].dir[2]= dirz;

    //input[i].dir[0]=dirx;
    //input[i].dir[1]=diry;
    //input[i].dir[2]=dirz;

//
    cout <<"dirx is: "<<dirx<<endl;
    cout <<"diry is: "<<diry<<endl;
    cout <<"dirz is: "<<dirz<<endl;
    cout <<endl;
}


	// allocate space for input/output/golden
	

//vector<uint32_t,boost::alignment::aligned_allocator<uint32_t,128>> golden(Ndw), input(Ndw), output(3*Ndw,0);
//vector<number,boost::alignment::aligned_allocator<number,128>> golden(Ndw), input(Ndw), output(3*Ndw);
//vector<packet,boost::alignment::aligned_allocator<input,128>> golden(Ndw), input(Ndw), output(3*Ndw);

cout <<"addresses of input "<<endl;
cout << &input[0] << "," << &input[1] << "," << &input[2] << endl;
cout <<"addresses of golden:"<< endl; 
cout << &golden[0] << "," << &golden[1] << "," << &golden[2] << endl;
cout <<endl;



//        vector<number> golden(Ndw), input(Ndw), output(3*Ndw);

#ifdef DOUBLE_BUFFERED
	// buffers for double-buffered transfer
        int bufferNum = 0;
        vector<
		uint64_t,
		boost::alignment::aligned_allocator<uint64_t,128>> buffer0(bufferSize), buffer1(bufferSize);

	assert(boost::alignment::is_aligned(128,buffer0.data()));
	assert(boost::alignment::is_aligned(128,buffer1.data()));
#endif

	assert(boost::alignment::is_aligned(128,golden.data()));
	assert(boost::alignment::is_aligned(128,input.data()));
	assert(boost::alignment::is_aligned(128,output.data()));

//boost::random::mt19937 rng0;
//cout << "rng0 is : "<<rng0 <<endl;
//vector<uint32_t> launch(100,0);
//boost::generate (launch, rng0);
ofstream os0 ("launch_dir.hex");
for (const auto go: launch)
{
     os0 << go.dir[0] << endl;
     os0 << go.dir[1] << endl;
     os0 << go.dir[2] << endl;  
}
os0.close();


for (int j=0; j<Ndw; j++)
{
  //golden[j].first= j*0.00001;
  //golden[j].second= (j+1)*0.00001;
  golden[j].first=128*j;
  golden[j].second=128*j*2;

  // golden[j]=0.00001*j;
}

 boost::copy(golden, input.begin());

	// generate stimulus
	//boost::random::mt19937 rng;

//cout <<"rng is : "<< rng<< endl; 
//boost::generate(golden, 123);
//	boost::generate(golden,  (std::ref(rng)));
//for (int yas=0; yas<Ndw; yas++)
//golden[yas]=uint64_t(1234);
//{
//golden[yas].first= uint64_t(1234);
//golden[yas].second= uint64_t(5678);
//}
/*
for (int k=0; k<Ndw; k++)
 {
  cout << "input.first:" << input[k].first << endl;
 cout << "input.second:" << input[k].second << endl; 
  cout << "golden.first: "<< golden[k].first << endl;
   cout << "golden.second: "<< golden[k].second << endl;
  }
*/

//       boost::random::mt19937 rng2;


//	boost::copy(golden, input.begin());

	ofstream os("golden.hex");
	for(const auto w : golden)
{
          //os << setw(16) << hex << w << endl;

		os << setw(16) << hex << w.first << endl;
                os << setw(16) << hex << w.second << endl;
}
	os.close();

	AFU afu(DEVICE_STRING);

    StackWED<MemcopyWED,128,128> wed;

#ifdef DOUBLE_BUFFERED
    // load first input buffer
    for (unsigned i=0; i<bufferSize; i++) {
            if (bufferNum == 0) {
                    buffer0[i] = input[i].first;
              // buffer0[i] = input[i];
            }
            else {
                    bool wrongBuffer = false;
                    assert(wrongBuffer);
                    buffer1[i] = input[i].first;
                    //buffer1[i] = input[i];
            }
    }

    wed->addr_from0 = (uint64_t)buffer0.data();
    wed->addr_from1 = (uint64_t)buffer1.data();
    wed->addr_to = (uint64_t)(output.data()+Ndw);
    wed->size = NbytesPerCopy;
    wed->buffered = 1;
#else
    wed->addr_from0 = (uint64_t)input.data();
    wed->addr_from1 = 0;
    wed->addr_to = (uint64_t)(output.data()+Ndw);
    wed->size = NbytesPerCopy;
    wed->buffered = 0;
#endif

#ifdef DOUBLE_BUFFERED
    cout << "Buf0: " << hex << setw(16) << wed->addr_from0 << endl;
    cout << "Buf1: " << hex << setw(16) << wed->addr_from1 << endl;
    cout << "  To: " << hex << setw(16) << wed->addr_to << endl;
    cout << "Size: " << hex << setw(16) << wed->size << endl;
    cout << "Double buffered" << endl;
#else
    cout << "From: " << hex << setw(16) << wed->addr_from0 << endl;
    cout << "  To: " << hex << setw(16) << wed->addr_to << endl;
    cout << "Size: " << hex << setw(16) << wed->size << endl;
    cout << "Not buffered" << endl;
#endif

    afu.start(wed.get(), 2);

    unsigned long long st=0;

    // blocking call, waits for interrupt corresponding to Waiting signal
    //afu.process_event_blocking ();

    // instead of interrupt, poll for Waiting status
	unsigned N;
	for(N=0;N<100 && (st=afu.mmio_read64(0)) != STATUS_WAITING;++N)
	{
		cout << "  Waiting for 'waiting' status (st=" << st << " looking for " << STATUS_WAITING << ")" << endl;
		usleep(100);
	}

    for(unsigned i=0;i<4;++i)
        cout << "i = " << i << ", MMIO[" << setw(6) << hex << (i<<3) << "] " << setw(16) << hex << afu.mmio_read64(i<<3) << endl;

    afu.mmio_write64(0,0x0ULL);		// start signal: write 0 to MMIO 0

#ifdef DOUBLE_BUFFERED
    uint64_t idx_counter;
    for (idx_counter=0; idx_counter < Ndw; idx_counter += bufferSize) {
    	afu.print_details();

        cout << "Starting copy " << idx_counter / bufferSize << ", input is on buffer " << bufferNum << endl;

        // load input buffer for next copy
        if (idx_counter + bufferSize < Ndw) {
            if (bufferNum == 1) {
                cout << "Copying to buffer 0...  ";
                for (unsigned i=0; i<bufferSize; i++) {
                    buffer0[i] = input[bufferSize + idx_counter + i];
                }
                cout << "Finished." << endl;

                bufferNum = 0;
            }
            else {
                cout << "Copying to buffer 1...  ";
                for (unsigned i=0; i<bufferSize; i++) {
                    buffer1[i] = input[bufferSize + idx_counter + i];
                }
                cout << "Finished." << endl;

                bufferNum = 1;
            }
        }
        
        // blocking call, waits for interrupt
        afu.process_event_blocking ();
        
        if (idx_counter + bufferSize < Ndw) {
            cout << "Continuing" << endl;
	        afu.mmio_write64(0,0x1ULL);
        }
        else {
            cout << "Terminating" << endl;
	        afu.mmio_write64(0,0x2ULL);
	        cout << "MMIO Written" << endl;
        }
	}
#else
    uint64_t byte_counter;
    for (byte_counter=0; byte_counter < Nbytes; byte_counter += NbytesPerCopy) {
        cout << "Starting copy " << byte_counter/NbytesPerCopy << endl;

            afu.mmio_write64(0,0x0ULL);		// start signal: write 0 to MMIO 0

            // blocking call, waits for interrupt
            afu.process_event_blocking ();
    
            if (byte_counter + NbytesPerCopy < Nbytes) {
                cout << "Continuing" << endl;
    	        afu.mmio_write64(0,0x1ULL);
            }
            else {
                cout << "Terminating" << endl;
    	        afu.mmio_write64(0,0x2ULL);
        }
	}
#endif

	bool ok=true;

	// check for correct copy and no corruption
/*
	for(size_t i=0;i<Ndw;++i)
		if (golden[i] != input[i])
		{
			ok = false;
			cerr << "Corrupted input data a/ " << setw(16) << hex << i << endl;
		}

	size_t i;

	for(i=0; i<Ndw; ++i)
		if (output[i] != 0)
		{
			ok = false;
			cerr << "Corrupted output data at output offset " << setw(16) << i << hex << endl;
		}
*/
size_t i;
	
for(i=Ndw;i<2*Ndw;++i)
		if (output[i].first != golden[i-Ndw].first)
		{
			ok = false;
			cerr << "Mismatch at " << setw(16) << hex << i-Ndw << " expecting " << setw(16) << golden[i-Ndw].first << " got " << setw(16) << output[i].first << endl;
		}
         for(i=Ndw;i<2*Ndw;++i)
                 if (output[i].second != golden[i-Ndw].second)
                 {
                         ok = false;
                         cerr << "Mismatch at " << setw(16) << hex << i-Ndw << " expecting " << setw(16) << golden[i-Ndw].second << " got " << setw(16) << output[i].second << endl;
                 }

       /* for(i=Ndw;i<2*Ndw;++i)
                 if (output[i] != golden[i-Ndw])
                 {
                         ok = false;
                         cerr << "Mismatch at " << setw(16) << hex << i-Ndw << " expecting " << setw(16) << golden[i-Ndw] << " got " << setw(16) << output[i] << endl;
                 }
*/


/*
	for(;i<3*Ndw;++i)
		if (output[i] != 0)
		{
			ok = false;
			cerr << "Corrupted output data at output offset " << setw(16) << hex << i << endl;
		}
*/
	// write output data to file
	os.open("output.hex");
	//for(size_t i=Ndw; i<2*Ndw; ++i)
	//	os << setw(16) << hex << output[i] << endl;

	if (ok)
		cout << "Checks passed!" << endl;

	return ok ? 0 : -1;

}
