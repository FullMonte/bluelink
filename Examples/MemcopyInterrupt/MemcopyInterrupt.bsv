// This file is part of BlueLink, a Bluespec library supporting the IBM CAPI coherent POWER8-FPGA link
// github.com/jeffreycassidy/BlueLink
//
// Copyright 2014 Jeffrey Cassidy
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package MemcopyInterrupt;

// Copies blocks of memory, specified by host code as a start address to copy from,
// a start address to copy to, and an increment (size of each block to copy). First
// copies one block and increments the from and to addresses and triggers an interrupt.
// Depending on MMIO response from host code, either copies the next block or terminates.


import DReg::*;

import Stream::*;
import WriteStream::*;
import ReadStream::*;

import AFU::*;
import AFUHardware::*;
import StmtFSM::*;
import PSLTypes::*;

import MMIO::*;
import FIFOF::*;
import Endianness::*;
import DedicatedAFU::*;
import Reserved::*;
import Vector::*;


import CmdArbiter::*;

import AFUShims::*;
import ConfigReg::*;

import CmdTagManager::*;

import ProgrammableLUT::*;
import SynthesisOptions::*;

typeclass ToReadOnly#(type t,type ifc);
    function ReadOnly#(t) toReadOnly(ifc i);
endtypeclass

instance ToReadOnly#(t,Reg#(t));
    function ReadOnly#(t) toReadOnly(Reg#(t) i) = regToReadOnly(i);
endinstance

instance ToReadOnly#(t,t);
    function ReadOnly#(t) toReadOnly(t i) = interface ReadOnly;
        method t _read = i;
    endinterface;
endinstance

// TODO: Is the comment below still valid?

/***********************************************************************************************************************************
 * Memcopy AFU
 *
 * Fixed buffer read latency of 1, no parity generation or checking
 *
 * Does not handle faults (page, address/data error)
 */


function EAddress64 offset(EAddress64 addr,UInt#(n) o) provisos (Add#(n,__some,64));
    return EAddress64 { addr: addr.addr + extend(o) };
endfunction

typedef struct {
    Integer     nReadTags;
    Integer     nReadBuf;
    Integer     nWriteTags;
    Integer     nWriteBuf;
    Bool        verbose;
    Bool        verboseData;
} Config;

typedef struct {
    LittleEndian#(EAddress64) addrFrom0; // 2 addresses for double buffering. In case of block transfer, only addrFrom0 is used
    LittleEndian#(EAddress64) addrFrom1;
    LittleEndian#(EAddress64) addrTo;
    LittleEndian#(UInt#(64))  size; // number of bytes to copy in each iteration
    LittleEndian#(UInt#(64))  buffered; // 1 if we are using double-buffering, 0 if we are doing block transfer

    Reserved#(704)  resv;
} WED deriving(Bits);



typedef enum { Unknown, Resetting, Ready, WEDRead, Waiting, Running, Done } Status deriving(Eq,FShow,Bits);

module [ModuleContext#(ctxT)] mkMemcopyInterrupt#(Config cfg)(DedicatedAFU#(2))
    provisos (
        Gettable#(ctxT,SynthesisOptions));

    // WED
    Vector#(2,Reg#(Bit#(512))) wedSegs <- replicateM(mkConfigReg(0));
    WED wed = concatSegReg(wedSegs,LE);

    // Start addresses for source and destination for each block copy
    Reg#(EAddress64) addrFrom       <- mkReg(0);
    Reg#(EAddress64) addrTo         <- mkReg(0);
    UInt#(64) size                  = unpackle(wed.size);

    // Whether or not to use double buffering
    Bool buffered = (unpackle(wed.buffered) == 1);
    // Current buffer id for double buffering
    Reg#(UInt#(1)) bufferNum <- mkReg(0);
    EAddress64 buffer0 = unpackle(wed.addrFrom0);
    EAddress64 buffer1 = unpackle(wed.addrFrom1);

    // Command-tag management
    CmdTagManagerUpstream#(2) pslside;
    CmdTagManagerClientPort#(Bit#(8)) tagmgr;

    { pslside, tagmgr } <- mkCmdTagManager(64);
    //{ pslside, tagmgr } <- mkCmdTagManager(32);
    Vector#(2,CmdTagManagerClientPort#(Bit#(8))) client <- mkCmdPriorityArbiter(tagmgr);

    // Stream controllers
    GetS#(Bit#(512)) idata;
    StreamCtrl istream;
    { istream, idata } <- mkReadStream(
        StreamConfig {
            bufDepth: cfg.nReadBuf,
            nParallelTags: cfg.nReadTags },
        client[1]);

    // Internal signals (can't use pulse wires for everything because interrupts take undetermined amount of time to complete)
    let pwWEDReady <- mkPulseWire;
    Reg#(Bool) startSignal <- mkReg(False);
    Reg#(Bool) continueCopy <- mkReg(True);
    Reg#(Bool) continueCopySignal <- mkReg(False);
    FIFOF#(void) finishedOneCopy <- mkGFIFOF1(True, False);

    Wire#(AFUReturn) ret <- mkWire;

    Put#(Bit#(512)) odata;
    StreamCtrl ostream;
    { ostream, odata } <- mkWriteStream(
        StreamConfig {
            nParallelTags: cfg.nWriteTags,
            bufDepth: cfg.nWriteBuf
        },
        client[0]);

    // to hold the tag assigned to an interrupt
    Reg#(RequestTag) interruptTag <- mkReg(0);
    // to hold the irq line chosen for an interrupt
    Reg#(EAddress64) irqLine <- mkReg(0);
    Reg#(Bool) interruptRequested <- mkDReg(False);
    Reg#(RequestTag) responseTag <- mkRegU();
    Reg#(Bool) interruptAcknowledged <- mkReg(False);

    // sends interrupt (NOTE: shouldn't be used during streaming process as it could conflict with cache commands
    // from the stream. Safe to use to indicate a stream is done)
    function Action requestInterrupt(EAddress64 irq);
        action
            irqLine <= irq;
            interruptRequested <= True;
        endaction
    endfunction

    (* descending_urgency="processInterruptRequest, issueWrite" *)
    rule processInterruptRequest if (interruptRequested);
        $display("Interrupt rule activated");
        let tag <- tagmgr.issue(CmdWithoutTag { com: Intreq,  cea: irqLine, csize: 128, cabt: Strict }, 0);
        interruptTag <= tag;
    endrule

    //  Master state machine
    Reg#(Status) st <- mkReg(Resetting);
    Stmt masterstmt = seq
        st <= Resetting;

        st <= Ready;

        action
            await(pwWEDReady);
            $display($time," INFO: Read WED");
            $display($time,"      Src address:       %016X",unpackle(wed.addrFrom0).addr);
            $display($time,"      Dst address:       %016X",unpackle(wed.addrTo).addr);
            $display($time,"      Size per transfer: %016X",unpackle(wed.size));

            addrFrom <= unpackle(wed.addrFrom0);
            addrTo <= unpackle(wed.addrTo);

            st <= Waiting;
        endaction

        action
            //st <= Running;            // for some reason this causes a scheduling error?
            await(startSignal);
        endaction

        //continueCopy <= True;
        while (continueCopy == True)
        seq
            $display("Streaming copy of size %016X from %016X to %016X", unpackle(wed.size), addrFrom, addrTo);
        
            // so that the control logic for continuing the loop is separated from the implicit
            // conditions on istream.start
            noAction;

            // do one block of copying using stream interface
            istream.start(addrFrom,unpackle(wed.size));
            ostream.start(addrTo,  unpackle(wed.size));

            action
                await(istream.done);
                $display($time," INFO: Read stream complete");
            endaction

            action
                await(ostream.done);
                $display($time," INFO: Write stream complete");
            endaction

            $display("About to request interrupt");

            // trigger interrupt to signal that copy is done
            requestInterrupt('h1);
            
            // catch interrupt response
            while (!interruptAcknowledged) seq
                responseTag <= tpl_1(tagmgr.response).rtag;
                interruptAcknowledged <= (responseTag == interruptTag);

                $display("Cache response received (may or may not be the response to the interrupt request)");
            endseq

            // reset register once interrupt has been caught
            interruptAcknowledged <= False;

            $display("Done signal received for Complete status interrupt");
            
            // wait for MMIO to determine whether to continue (processed in rule detectContinue)
            finishedOneCopy.deq;
            continueCopy <= continueCopySignal;
            
            action
                if (buffered) begin
                    // swap buffers
                    if (bufferNum == 0) begin
                        $display("Switching from buffer 0 to buffer 1");
                        addrFrom <= buffer1;
                        bufferNum <= 1;
                    end
                    else begin
                        $display("Switching from buffer 1 to buffer 0");
                        addrFrom <= buffer0;
                        bufferNum <= 0;
                    end
                end
                else begin
                    addrFrom <= offset(addrFrom, unpackle(wed.size));
                end

                addrTo <= offset(addrTo, unpackle(wed.size));
            endaction
        endseq

        action
            st <= Done;
            ret <= Done;
        endaction
    endseq;

    let masterfsm <- mkFSM(masterstmt);

    rule getOutput;
        let d = idata.first;
        idata.deq;
        if (cfg.verboseData)
        begin
            $write($time," INFO: Received data ");
            for(Integer i=7;i>=0;i=i-1)
            begin
                $write("%016X ",endianSwap((Bit#(64))'(d[i*64+63:i*64])));
                if (i % 4 == 0)
                begin
                    $display;
                    if (i > 0)
                        $write("                                       ");
                end
            end
        end

        odata.put(d);
    endrule
    
    FIFOF#(MMIOResponse) mmResp <- mkGFIFOF1(True,False);

    interface ClientU command = pslside.command;
    interface AFUBufferInterface buffer = pslside.buffer;

    interface Server mmio;
        interface Get response = toGet(mmResp);

        interface Put request;
            method Action put(MMIORWRequest mm);
                case (mm) matches
                    tagged DWordWrite { index: 0, data: 0 }:
                        action
                            startSignal <= True;
                            mmResp.enq(64'h0);
                        endaction
                    tagged DWordWrite { index: 0, data: 1 }:
                        action
                            finishedOneCopy.enq(?);
                            continueCopySignal <= True;
                            mmResp.enq(64'h0);
                        endaction
                    tagged DWordWrite { index: 0, data: 2 }:
                        action
                            finishedOneCopy.enq(?);
                            continueCopySignal <= False;
                            mmResp.enq(64'h0);
                        endaction
                    tagged DWordRead  { index: .i }:
                        mmResp.enq(case(i) matches
                            0: case(st) matches
                                    Resetting: 0;
                                    Ready: 1;
                                    Waiting: 2;
                                    Running: 3;
                                    Done: 4;
                                endcase
                            1: pack(unpackle(wed.size));
                            2: pack(unpackle(wed.addrFrom0).addr);
                            3: pack(unpackle(wed.addrTo).addr);
                            default: 64'hdeadbeefbaadc0de;
                        endcase);
                    default:
                        mmResp.enq(64'h0);
                endcase
            endmethod
        endinterface
    endinterface

    method Action wedwrite(UInt#(6) i,Bit#(512) val) = asReg(wedSegs[i])._write(val);

    method Action rst = masterfsm.start;
    method Bool rdy = (st == Ready);

    method Action start(EAddress64 ea, UInt#(8) croom) = pwWEDReady.send;
    method ActionValue#(AFUReturn) retval = actionvalue return ret; endactionvalue;
endmodule


(* clock_prefix="ha_pclock" *)
module [Module] mkMemcopyInterruptAFU(AFUHardware#(2));
	// synthesis options is a struct of things like what hardware to synthesize to, how verbose errors should be etc.
    SynthesisOptions syn = defaultValue;

    Config cfg = Config {
        verboseData: False,
        verbose: True,
        nReadTags: 32,
        nReadBuf: 32,
        nWriteBuf: 32,
        nWriteTags: 32
        };

    // this essentially passes syn to mkMemcopyInterrupt as a "background" struct, which is implicitly passed to later modules
    // such as the streaming interfaces. This means syn is always available to child modules that are of the appropriate
    // ModuleContext type
    let { ctx, dut } <- runWithContext(
        hCons(syn,hNil),
        mkMemcopyInterrupt(cfg)
    );

    // dut is the DedicatedAFU# interface provided by mkMemcopyInterrupt, afu is the AFU# interface
    // (DedicatedAFU is a middleware that provides an easier to use interface, without dealing as directly with technicalities)
    let afu <- mkDedicatedAFU(dut, 0);

    // finally, produces the hardware wrapper which directly matches pin names set by CAPI interface
    AFUHardware#(2) hw <- mkCAPIHardwareWrapper(afuParityWrapper(afu));
    return hw;
endmodule

endpackage
