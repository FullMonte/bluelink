/*
 * host_memcopy.c
 *
 *  Created on: Mar 25, 2015
 *      Author: jcassidy
 */

#include <cinttypes>
#include <boost/random/mersenne_twister.hpp>

#include <boost/align/aligned_allocator.hpp>

#include <boost/range.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/adaptor/indexed.hpp>

#include <BlueLink/Host/AFU.hpp>
#include <BlueLink/Host/WED.hpp>

#include <iostream>
#include <iomanip>
#include <functional>
#include <fstream>
#include <vector>

#define DEVICE_STRING "/dev/cxl/afu0.0d"

struct MemcopyWED {
	uint64_t	addr_from0; // start address of array to copy from. 2 addresses for double buffering. In case of block transfer, only addrFrom0 is used
	uint64_t	addr_from1;
	uint64_t	addr_to; // start address of array to copy to
	uint64_t	size; // number of bytes to copy in each iteration
	uint64_t        buffered; // 1 if we are using double-buffering, 0 if we are doing block transfer

	uint64_t	resv[11];
};

#define STATUS_READY 0x1ULL
#define STATUS_WAITING 0x2ULL
#define STATUS_RUNNING 0x3ULL
#define STATUS_DONE 0x4ULL

//#define DOUBLE_BUFFERED
#define HARDWARE

using namespace std;

int main (int argc, char *argv[])
{
#ifdef HARDWARE
	const bool sim = false;
#else
	const bool sim = true;
#endif
	const size_t Nbytes=65536;
	//const size_t Nbytes=32768;
    //const size_t Nbytes=16384;
	//const size_t NbytesPerCopy=65536;
	const size_t NbytesPerCopy=512;
    //const size_t NbytesPerCopy=4096;
	const size_t Ndw=Nbytes/8;
	const size_t bufferSize=NbytesPerCopy/8;
//struct alignas(128) number
 //{
//uint64_t first;
//uint64_t second;
//};
	// allocate space for input/output/golden
	vector<
		uint64_t,
		boost::alignment::aligned_allocator<uint64_t,128>> golden(Ndw), input(Ndw), output(3*Ndw,0);

//        vector<number> golden(Ndw), input(Ndw), output(3*Ndw);

#ifdef DOUBLE_BUFFERED
	// buffers for double-buffered transfer
        int bufferNum = 0;
        vector<
		uint64_t,
		boost::alignment::aligned_allocator<uint64_t,128>> buffer0(bufferSize), buffer1(bufferSize);

	assert(boost::alignment::is_aligned(128,buffer0.data()));
	assert(boost::alignment::is_aligned(128,buffer1.data()));
#endif

	assert(boost::alignment::is_aligned(128,golden.data()));
	assert(boost::alignment::is_aligned(128,input.data()));
	assert(boost::alignment::is_aligned(128,output.data()));

	// generate stimulus
	boost::random::mt19937_64 rng;
//boost::generate(golden, 123);
	boost::generate(golden, std::ref(rng));
//for (int yas=0; yas<Ndw; yas++)
//golden[yas]=uint64_t(1234);
//{
//golden[yas].first= uint64_t(1234);
//golden[yas].second= uint64_t(5678);
//}

	boost::copy(golden, input.begin());

	ofstream os("golden.hex");
	for(const auto w : golden)
{
          os << setw(16) << hex << w << endl;

		//os << setw(16) << hex << w.first << endl;
                //os << setw(16) << hex << w.second << endl;
}
	os.close();

	AFU afu(DEVICE_STRING);

    StackWED<MemcopyWED,128,128> wed;

#ifdef DOUBLE_BUFFERED
    // load first input buffer
    for (unsigned i=0; i<bufferSize; i++) {
            if (bufferNum == 0) {
                    //buffer0[i] = input[i].first;
               buffer0[i] = input[i];
            }
            else {
                    bool wrongBuffer = false;
                    assert(wrongBuffer);
                    //buffer1[i] = input[i].first;
                    buffer1[i] = input[i];
            }
    }

    wed->addr_from0 = (uint64_t)buffer0.data();
    wed->addr_from1 = (uint64_t)buffer1.data();
    wed->addr_to = (uint64_t)(output.data()+Ndw);
    wed->size = NbytesPerCopy;
    wed->buffered = 1;
#else
    wed->addr_from0 = (uint64_t)input.data();
    wed->addr_from1 = 0;
    wed->addr_to = (uint64_t)(output.data()+Ndw);
    wed->size = NbytesPerCopy;
    wed->buffered = 0;
#endif

#ifdef DOUBLE_BUFFERED
    cout << "Buf0: " << hex << setw(16) << wed->addr_from0 << endl;
    cout << "Buf1: " << hex << setw(16) << wed->addr_from1 << endl;
    cout << "  To: " << hex << setw(16) << wed->addr_to << endl;
    cout << "Size: " << hex << setw(16) << wed->size << endl;
    cout << "Double buffered" << endl;
#else
    cout << "From: " << hex << setw(16) << wed->addr_from0 << endl;
    cout << "  To: " << hex << setw(16) << wed->addr_to << endl;
    cout << "Size: " << hex << setw(16) << wed->size << endl;
    cout << "Not buffered" << endl;
#endif

    afu.start(wed.get(), 2);

    unsigned long long st=0;

    // blocking call, waits for interrupt corresponding to Waiting signal
    //afu.process_event_blocking ();

    // instead of interrupt, poll for Waiting status
	unsigned N;
	for(N=0;N<100 && (st=afu.mmio_read64(0)) != STATUS_WAITING;++N)
	{
		cout << "  Waiting for 'waiting' status (st=" << st << " looking for " << STATUS_WAITING << ")" << endl;
		usleep(100);
	}

    for(unsigned i=0;i<4;++i)
        cout << "i = " << i << ", MMIO[" << setw(6) << hex << (i<<3) << "] " << setw(16) << hex << afu.mmio_read64(i<<3) << endl;

    afu.mmio_write64(0,0x0ULL);		// start signal: write 0 to MMIO 0

#ifdef DOUBLE_BUFFERED
    uint64_t idx_counter;
    for (idx_counter=0; idx_counter < Ndw; idx_counter += bufferSize) {
    	afu.print_details();

        cout << "Starting copy " << idx_counter / bufferSize << ", input is on buffer " << bufferNum << endl;

        // load input buffer for next copy
        if (idx_counter + bufferSize < Ndw) {
            if (bufferNum == 1) {
                cout << "Copying to buffer 0...  ";
                for (unsigned i=0; i<bufferSize; i++) {
                    buffer0[i] = input[bufferSize + idx_counter + i];
                }
                cout << "Finished." << endl;

                bufferNum = 0;
            }
            else {
                cout << "Copying to buffer 1...  ";
                for (unsigned i=0; i<bufferSize; i++) {
                    buffer1[i] = input[bufferSize + idx_counter + i];
                }
                cout << "Finished." << endl;

                bufferNum = 1;
            }
        }
        
        // blocking call, waits for interrupt
        afu.process_event_blocking ();
        
        if (idx_counter + bufferSize < Ndw) {
            cout << "Continuing" << endl;
	        afu.mmio_write64(0,0x1ULL);
        }
        else {
            cout << "Terminating" << endl;
	        afu.mmio_write64(0,0x2ULL);
	        cout << "MMIO Written" << endl;
        }
	}
#else
    uint64_t byte_counter;
    for (byte_counter=0; byte_counter < Nbytes; byte_counter += NbytesPerCopy) {
        cout << "Starting copy " << byte_counter/NbytesPerCopy << endl;

            afu.mmio_write64(0,0x0ULL);		// start signal: write 0 to MMIO 0

            // blocking call, waits for interrupt
            afu.process_event_blocking ();
    
            if (byte_counter + NbytesPerCopy < Nbytes) {
                cout << "Continuing" << endl;
    	        afu.mmio_write64(0,0x1ULL);
            }
            else {
                cout << "Terminating" << endl;
    	        afu.mmio_write64(0,0x2ULL);
        }
	}
#endif

	bool ok=true;

	// check for correct copy and no corruption
	for(size_t i=0;i<Ndw;++i)
		if (golden[i] != input[i])
		{
			ok = false;
			cerr << "Corrupted input data at " << setw(16) << hex << i << endl;
		}

	size_t i;

	for(i=0; i<Ndw; ++i)
		if (output[i] != 0)
		{
			ok = false;
			cerr << "Corrupted output data at output offset " << setw(16) << i << hex << endl;
		}

	for(;i<2*Ndw;++i)
		if (output[i] != golden[i-Ndw])
		{
			ok = false;
			cerr << "Mismatch at " << setw(16) << hex << i-Ndw << " expecting " << setw(16) << golden[i-Ndw] << " got " << setw(16) << output[i] << endl;
		}

	for(;i<3*Ndw;++i)
		if (output[i] != 0)
		{
			ok = false;
			cerr << "Corrupted output data at output offset " << setw(16) << hex << i << endl;
		}

	// write output data to file
	os.open("output.hex");
	for(size_t i=Ndw; i<2*Ndw; ++i)
		os << setw(16) << hex << output[i] << endl;

	if (ok)
		cout << "Checks passed!" << endl;

	return ok ? 0 : -1;
}
