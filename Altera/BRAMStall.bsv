package BRAMStall;

import Assert::*;

import BRAM::*;
import BRAMCore::*;
import SpecialFIFOs::*;
import PAClib::*;
import GetPut::*;
import FIFOF::*;

/** Unguarded primitive BRAM port with read data stall capability.
 * Read data will be available on readdata port as soon as it's available, and will be held until deq is called.
 *
 * Underlying BRAM has 2 stages of latency (input address reg, output data reg). The registers are clock-enabled if
 * putcmd or deq is called.
 */

interface BRAM_PORT_Stall_PrimUG#(type addrT,type dataT);
    (* always_ready *)
    method Action           putcmd(Bool wr,addrT addr,dataT data);

    (* always_ready *)
    method dataT            readdata;

    (* always_ready *)
    method Action   deq;

    (* always_ready *)
    method Action   clear;
endinterface

interface BRAM_DUAL_PORT_Stall_PrimUG#(type addrT,type dataT);
    interface BRAM_PORT_Stall_PrimUG#(addrT,dataT) a;
    interface BRAM_PORT_Stall_PrimUG#(addrT,dataT) b;
endinterface




interface BRAMPortStall#(type addrT,type dataT);
    method Action putcmd(Bool wr,addrT addr,dataT data);
    method Action clear;

    interface PipeOut#(dataT)       readdata;
endinterface

interface BRAM2PortStall#(type addrT,type dataT);
    interface BRAMPortStall#(addrT,dataT)   porta;
    interface BRAMPortStall#(addrT,dataT)   portb;
endinterface



/** Wraps the primitive above to produce a BRAM interface with pipeout read data. 
 * Commands can be issued when there is space in the command reg.
 * Writes always vacate the command reg immediately.
 * Reads vacate the command reg when there is space in the output reg.
 *
 * Behaviour uses a pipeline FIFO to get full throughput, so readdata.deq schedules before putcmd.
 */


module mkBRAMStallPipeOut#(BRAM_PORT_Stall_PrimUG#(addrT,dataT) brport)(BRAMPortStall#(addrT,dataT))
    provisos (
        Bits#(addrT,na),
        Bits#(dataT,nd));
    FIFOF#(Bool)        ramiread   <- mkLFIFOF;     // True => input command is read, False => write, empty => no command
    FIFOF#(void)        oValid     <- mkLFIFOF;     // full if data present at read port

    // allow read data to propagate to output reg if there is space
    rule advanceWhenAble if (ramiread.first);
        oValid.enq(?);
        ramiread.deq;
        brport.deq;
    endrule

    // if it's not a read, we don't care about the output so can safely discard
    rule discardWrite if (!ramiread.first);
        ramiread.deq;
    endrule

    method Action putcmd(Bool wr,addrT addr,dataT data);
        ramiread.enq(!wr);
        brport.putcmd(wr,addr,data);
    endmethod

    method Action clear;
        oValid.clear;
        ramiread.clear;
        brport.clear;
    endmethod

    interface PipeOut readdata;
        method Bool notEmpty = oValid.notEmpty;
        method Action deq;
            oValid.deq;
        endmethod

        method dataT first if (oValid.notEmpty) = brport.readdata;
    endinterface
endmodule


function BRAMPortStall#(a,d) portA(BRAM2PortStall#(a,d) br) = br.porta;
function BRAMPortStall#(a,d) portB(BRAM2PortStall#(a,d) br) = br.portb;

function PipeOut#(d) portReadPipe(BRAMPortStall#(a,d) brp)  = brp.readdata;
function PipeOut#(d) portAReadPipe(BRAM2PortStall#(a,d) br) = br.porta.readdata;
function PipeOut#(d) portBReadPipe(BRAM2PortStall#(a,d) br) = br.portb.readdata;


// free functions for map/mapM
function Action doPutCmd(Bool wr,a addr,d data,BRAMPortStall#(a,d) _br) = _br.putcmd(wr,addr,data);
function Action doClear(BRAMPortStall#(a,d) _br) = _br.clear;
function Action doDeq(BRAMPortStall#(a,d) _br) = _br.readdata.deq;
function d      getFirst(BRAMPortStall#(a,d) _br) = _br.readdata.first;
function Bool   getNotEmpty(PipeOut#(t) p) = p.notEmpty;
function PipeOut#(d) getReadDataPipe(BRAMPortStall#(a,d) _br) = _br.readdata;



/** Bluespec functional model for BRAM2PortStall (actual implementation is Verilog BRAM2.Stall.v)
 *
 */

module mkBluespecBRAM2Core#(Integer depth)(BRAM_DUAL_PORT_Stall_PrimUG#(UInt#(na),dataT))
    provisos (Bits#(dataT,nbData));

    // the BRAM core (depth, outReg)
    BRAM_DUAL_PORT#(UInt#(na),dataT) br <- mkBRAMCore2(depth,False);

    Reg#(dataT) oDataA <- mkReg(?);
    Reg#(dataT) oDataB <- mkReg(?);

    let pwCEA <- mkPulseWireOR;
    let pwCEB <- mkPulseWireOR;

    (* no_implicit_conditions *)
    rule oRegA if (pwCEA);
        oDataA <= br.a.read;
    endrule

    (* no_implicit_conditions *)
    rule oRegB if (pwCEB);
        oDataB <= br.b.read;
    endrule

    interface BRAM_PORT_Stall_PrimUG a;
        method Action putcmd(Bool wr,UInt#(na) addr,dataT data);
            br.a.put(wr,addr,data);
            pwCEA.send;
        endmethod

        method dataT readdata = oDataA;
        method Action deq = pwCEA.send;
        method Action clear = oDataA._write(?);
    endinterface
    
    interface BRAM_PORT_Stall_PrimUG b;
        method Action putcmd(Bool wr,UInt#(na) addr,dataT data);
            br.b.put(wr,addr,data);
            pwCEB.send;
        endmethod

        method dataT readdata = oDataB;
        method Action deq = pwCEB.send;
        method Action clear = oDataB._write(?);
    endinterface
endmodule

module mkBluespecBRAM2WithStall#(Integer depth)(BRAM2PortStall#(UInt#(na),dataT))
    provisos (Bits#(dataT,nd));

    // instantiate an unguarded low-level BRAM
    BRAM_DUAL_PORT_Stall_PrimUG#(UInt#(na),dataT) _ram <- mkBluespecBRAM2Core(depth);


    let a <- mkBRAMStallPipeOut(_ram.a);
    let b <- mkBRAMStallPipeOut(_ram.b);

    interface BRAMPortStall porta = a;
    interface BRAMPortStall portb = b;
endmodule


/** Present a BRAM holding t as a BRAM holding Bit#(n) with wider data (n >= bitSize(t)) and address
 */

function BRAMPortStall#(UInt#(nbAddr),Bit#(nbChain)) padBRAMIfc(BRAMPortStall#(UInt#(nbOffset),dataT) br)
    provisos (
        Bits#(dataT,nbData),
        Add#(nbOffset,nbAddrPad,nbAddr),
        Add#(nbData,nbPad,nbChain)
        ) = 
    interface BRAMPortStall;
        method Action putcmd(Bool wr,UInt#(nbAddr) addr,Bit#(nbChain) wrdata) =
            br.putcmd(wr,truncate(addr),unpack(truncate(wrdata)));
        interface PipeOut readdata;
            method Action deq = br.readdata.deq;
            method Bit#(nbChain) first = extend(pack(br.readdata.first));
            method Bool notEmpty = br.readdata.notEmpty;
        endinterface
        method Action clear = br.clear;
    endinterface;


/** Gates a BRAM port on a boolean condition. If true, passes the requests/responses through; else, blocks.
 * When blocked, readdata.notEmpty and .first pass through as normal but .deq, putcmd, and clear block.
 */

function BRAMPortStall#(addrT,dataT) gateBRAMPort(Bool en,BRAMPortStall#(addrT,dataT) br) = interface BRAMPortStall;
    method Action putcmd(Bool wr,addrT addr,dataT data) if (en) = br.putcmd(wr,addr,data);
    method Action clear if (en) = br.clear;

    interface PipeOut readdata;
        method Bool notEmpty = br.readdata.notEmpty;
        method Action deq if (en) = br.readdata.deq;
        method dataT first if (en) = br.readdata.first;
    endinterface
endinterface;


endpackage
