# Provides a series of commands to add waveforms for the CAPI interface

proc capi_waves { path } {
    capi_waves_control      $path
    capi_waves_status       $path
    capi_waves_mmio         $path
	capi_waves_command	    $path
    capi_waves_buffer    $path
}

proc capi_waves_control { path } {
	add wave -group Control -noupdate                       -label ha_jval      ${path}ha_jval
	add wave -group Control -noupdate -radix hexadecimal    -label ha_jcom      ${path}ha_jcom
    add wave -group Control -noupdate                       -label ha_jcompar   ${path}ha_jcompar
	add wave -group Control -noupdate -radix hexadecimal    -label ha_jea       ${path}ha_jea
    add wave -group Control -noupdate                       -label ha_jeapar    ${path}ha_jeapar 
}

proc capi_waves_status { path } { 
	add wave -group Status -noupdate                        ${path}ah_jrunning
	add wave -group Status -noupdate                        ${path}ah_jdone
	add wave -group Status -noupdate                        ${path}ah_jcack
	add wave -group Status -noupdate                        ${path}ah_tbreq
	add wave -group Status -noupdate                        ${path}ah_paren
	add wave -group Status -noupdate                        ${path}ah_jyield
	add wave -group Status -noupdate -radix hexadecimal     ${path}ah_jerror
}

proc capi_waves_command { path } {
    capi_waves_command_request $path
    capi_waves_command_response $path
}

proc capi_waves_command_response { path } {
    add wave -group Command -group Response                     ${path}ha_rvalid
    add wave -group Command -group Response -radix hexadecimal  ${path}ha_rtag
    add wave -group Command -group Response                     ${path}ha_rtagpar
    add wave -group Command -group Response -radix hexadecimal  ${path}ha_response
    add wave -group Command -group Response -radix signed       ${path}ha_rcredits
    add wave -group Command -group Response -radix hexadecimal  ${path}ha_rcachestate
    add wave -group Command -group Response -radix hexadecimal  ${path}ha_rcachepos
}

proc capi_waves_command_request { path } {
    add wave -group Command -group Request                      ${path}ah_cvalid
    add wave -group Command -group Request -radix hexadecimal   ${path}ah_com
    add wave -group Command -group Request                      ${path}ah_compar
    add wave -group Command -group Request -radix hexadecimal   ${path}ah_ctag
    add wave -group Command -group Request                      ${path}ah_ctagpar
    add wave -group Command -group Request -radix hexadecimal   ${path}ah_cabt
    add wave -group Command -group Request -radix hexadecimal   ${path}ah_csize
    add wave -group Command -group Request -radix hexadecimal   ${path}ah_cea
    add wave -group Command -group Request                      ${path}ah_ceapar
    add wave -group Command -group Request -radix hexadecimal   ${path}ah_cch
}

proc capi_waves_mmio { path } {
    capi_waves_mmio_request $path
    capi_waves_mmio_response $path
}

proc capi_waves_mmio_request { path } {
	add wave -group MMIO -group Request -noupdate                       ${path}ha_mmval
	add wave -group MMIO -group Request -noupdate                       ${path}ha_mmcfg
	add wave -group MMIO -group Request -noupdate                       ${path}ha_mmrnw
	add wave -group MMIO -group Request -noupdate -radix hexadecimal    ${path}ha_mmdata
	add wave -group MMIO -group Request -noupdate -radix hexadecimal    ${path}ha_mmad
}

proc capi_waves_mmio_response { path } {
	add wave -group MMIO -group Response -noupdate                      ${path}ah_mmack
	add wave -group MMIO -group Response -noupdate -radix hexadecimal   ${path}ah_mmdata
	add wave -group MMIO -group Response -noupdate                      ${path}ah_mmdatapar
}

proc capi_waves_buffer { path } { 
    capi_waves_buffer_read $path
    capi_waves_buffer_write $path
}

proc capi_waves_buffer_read { path } {
    capi_waves_buffer_read_request $path
    capi_waves_buffer_read_response $path
    capi_waves_buffer_write $path
}

proc capi_waves_buffer_read_request { path } {
    add wave -group Buffer -group Read -group Request -noupdate                     ${path}ha_brvalid
    add wave -group Buffer -group Read -group Request -noupdate -radix unsigned     ${path}ha_brtag
    add wave -group Buffer -group Read -group Request -noupdate                     ${path}ha_brtagpar
    add wave -group Buffer -group Read -group Request -noupdate -radix unsigned     ${path}ha_brad
}

proc capi_waves_buffer_read_response { path } {
    global brlatcycles
    add wave -group Buffer -group Read -group Response -noupdate                    ${path}brvalid_delay
    add wave -group Buffer -group Read -group Response -noupdate -radix hexadecimal ${path}brtag_delay
    add wave -group Buffer -group Read -group Response -noupdate -radix hexadecimal ${path}ah_brdata
    add wave -group Buffer -group Read -group Response -noupdate -radix hexadecimal ${path}ah_brpar

    add wave -group Buffer -group Read -group Response -noupdate -radix unsigned    ${path}ah_brlat

    add wave -group Buffer -group Read -group Response -noupdate -radix hexadecimal ${path}ah_brpar
}

proc capi_waves_buffer_write { path } {
    add wave -group Buffer -group Write -noupdate                       ${path}ha_bwvalid
    add wave -group Buffer -group Write -noupdate -radix unsigned       ${path}ha_bwtag
    add wave -group Buffer -group Write -noupdate                       ${path}ha_bwtagpar
    add wave -group Buffer -group Write -noupdate -radix hexadecimal    ${path}ha_bwad
    add wave -group Buffer -group Write -noupdate -radix hexadecimal    ${path}ha_bwdata
    add wave -group Buffer -group Write -noupdate -radix hexadecimal    ${path}ha_bwpar
}


