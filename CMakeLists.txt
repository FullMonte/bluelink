PROJECT(BlueLink)
CMAKE_MINIMUM_REQUIRED(VERSION 3.0)

###### C++ options

IF(${CMAKE_VERSION} VERSION_LESS "3.1")
    ADD_DEFINITIONS(-std=c++11)
ELSE()
    SET(CMAKE_CXX_STANDARD 11)
ENDIF()

SET(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)

## Include the parent of the source dir to get BlueLink/... includes
INCLUDE_DIRECTORIES(${CMAKE_CURRENT_SOURCE_DIR}/..)

#FIND_PACKAGE(CAPI REQUIRED)
#INCLUDE(${CAPI_USE_FILE})

###### VSIM required for simulation with PSLSE but not host code

#OPTION(USE_VSIM ON)
IF(USE_VSIM)
	FIND_PACKAGE(VSIM REQUIRED)
	INCLUDE(${VSIM_USE_FILE})
ELSE()
	MESSAGE("VSIM disabled")
ENDIF()


###### Bluespec required to compile hardware
# Set up Bluespec and put object files in [...]/bdir

#OPTION(USE_Bluespec ON)
IF(USE_Bluespec)
	FIND_PACKAGE(Bluespec REQUIRED)
	INCLUDE(${Bluespec_USE_FILE})
ENDIF()

###### BDPIDevice CMake module
#OPTION(USE_BDPIDevice ON)
IF(USE_BDPIDevice)
	FIND_PACKAGE(BDPIDevice REQUIRED)
	INCLUDE(${BDPIDevice_USE_FILE})
ENDIF()




###### Config items that should be set in UseBlueLink.cmake as well
SET(BlueLink_HA_ASSIGNMENT_DELAY "#1" CACHE STRING "Assignment delay for host-to-AFU signals")


## Create the CMake package config file
SET(BlueLink_INCLUDE_DIRS               ${CMAKE_CURRENT_SOURCE_DIR}/..)
SET(BlueLink_LIBRARY_DIRS               ${CMAKE_BINARY_DIR}/lib)
SET(BlueLink_BDIR                       ${CMAKE_BINARY_DIR}/bdir)
SET(BlueLink_USE_FILE                   ${CMAKE_BINARY_DIR}/cmake/UseBlueLink.cmake)
SET(BlueLink_VERILOG_DIR				${CMAKE_CURRENT_SOURCE_DIR}/PSLVerilog)
SET(BlueLink_VSIM_ALTERA_LIBRARY        ${CMAKE_BINARY_DIR}/vsim/bsvaltera)
CONFIGURE_FILE(cmake/BlueLinkConfig.cmake.in  ${CMAKE_BINARY_DIR}/cmake/BlueLinkConfig.cmake)

SET(BlueLink_DIR                        ${CMAKE_BINARY_DIR})
SET(BlueLink_INCLUDE_DIRS               ${CMAKE_INSTALL_PREFIX}/include)
SET(BlueLink_LIBRARY_DIRS               ${CMAKE_INSTALL_PREFIX}/lib)
SET(BlueLink_BDIR                       ${CMAKE_INSTALL_PREFIX}/bdir)
SET(BlueLink_USE_FILE                   ${CMAKE_BINARY_DIR}/cmake/UseBlueLink.cmake)
SET(BlueLink_VERILOG_DIR				${CMAKE_INSTALL_PREFIX}/verilog)
CONFIGURE_FILE(cmake/BlueLinkConfig.cmake.in  ${CMAKE_BINARY_DIR}/cmake/BlueLinkConfig.install.cmake)





FILE(MAKE_DIRECTORY PSLVerilog ${CMAKE_BINARY_DIR})
FILE(COPY PSLVerilog/top.v PSLVerilog/revwrap_sim.v PSLVerilog/revwrap_syn.v DESTINATION ${CMAKE_BINARY_DIR}/PSLVerilog)

FILE(MAKE_DIRECTORY cmake ${CMAKE_BINARY_DIR})
FILE(COPY cmake/UseBlueLink.cmake DESTINATION ${CMAKE_BINARY_DIR}/cmake)



IF(Bluespec_FOUND)
	ADD_BSV_PACKAGE(CAPIOptions)
    ADD_BSV_PACKAGE(SynthesisOptions)
    ADD_BSV_PACKAGE(ShiftReg)
ENDIF()


FILE(MAKE_DIRECTORY pslse_config ${CMAKE_BINARY_DIR})
CONFIGURE_FILE(pslse_server.dat.in ${CMAKE_BINARY_DIR}/pslse_config/pslse_server.dat)
CONFIGURE_FILE(shim_host.dat.in ${CMAKE_BINARY_DIR}/pslse_config/shim_host.dat)
CONFIGURE_FILE(pslse.parms.in ${CMAKE_BINARY_DIR}/pslse_config/pslse.parms)

FILE(COPY ${CMAKE_BINARY_DIR}/pslse_config/pslse.parms   DESTINATION ${CMAKE_SOURCE_DIR}/External/fm-pslse/libcxl)
FILE(COPY ${CMAKE_BINARY_DIR}/pslse_config/pslse_server.dat DESTINATION ${CMAKE_SOURCE_DIR}/External/fm-pslse/libcxl)
FILE(COPY ${CMAKE_BINARY_DIR}/pslse_config/shim_host.dat DESTINATION ${CMAKE_SOURCE_DIR}/External/fm-pslse/pslse)

MESSAGE("${Blue}Entering subdirectory External/BlueLink/Altera${ColourReset}")
ADD_SUBDIRECTORY(Altera)
MESSAGE("${Blue}Entering subdirectory External/BlueLink/Core${ColourReset}")
ADD_SUBDIRECTORY(Core)
MESSAGE("${Blue}Entering subdirectory External/BlueLink/MMIO${ColourReset}")
ADD_SUBDIRECTORY(MMIO)
MESSAGE("${Blue}Entering subdirectory External/BlueLink/DedicatedAFU${ColourReset}")
ADD_SUBDIRECTORY(DedicatedAFU)
MESSAGE("${Blue}Entering subdirectory External/BlueLink/CommandInterface${ColourReset}")
ADD_SUBDIRECTORY(CommandInterface)
MESSAGE("${Blue}Entering subdirectory External/BlueLink/Stream${ColourReset}")
ADD_SUBDIRECTORY(Stream)
MESSAGE("${Blue}Entering subdirectory External/BlueLink/Host${ColourReset}")
ADD_SUBDIRECTORY(Host)
MESSAGE("${Blue}Entering subdirectory External/BlueLink/Examples${ColourReset}")
ADD_SUBDIRECTORY(Examples)
MESSAGE("${Blue}Entering subdirectory External/BlueLink/Support${ColourReset}")
ADD_SUBDIRECTORY(Support)
MESSAGE("${Blue}Entering subdirectory External/BlueLink/MemScanChain${ColourReset}")
ADD_SUBDIRECTORY(MemScanChain)