/*
 * MemRequestFromFile.hpp
 *
 *  Created on: Mar 8, 2017
 *      Author: jcassidy
 */

#ifndef MEMSCANCHAIN_MEMREQUESTFROMFILE_HPP_
#define MEMSCANCHAIN_MEMREQUESTFROMFILE_HPP_

#include "MemItem.hpp"
#include "MemScanChainTest.hpp"

#include <iostream>
#include <fstream>

/** Reads memory requests from a text file.
 *
 * File may contain:
 *
 * W <addr> <data>			Write request
 * R <addr>					Read request
 * I 						Idle
 * P <data>					Passthrough
 *
 *
 * Where <addr>, <data> are hex values.
 */

class MemRequestFromFile : public MemScanChainTest::MemRequestGenerator
{
public:
	MemRequestFromFile(const char* fn);

	virtual boost::optional<RequestType> operator()() 			override;
	virtual bool eof() 									const 	override;

private:
	std::string		m_filename;
	std::ifstream	m_is;
};


#endif /* MEMSCANCHAIN_MEMREQUESTFROMFILE_HPP_ */
