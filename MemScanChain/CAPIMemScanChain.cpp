/*
 * CAPIMemScanChain.cpp
 *
 *  Created on: Mar 9, 2017
 *      Author: jcassidy
 */

#include <BlueLink/Host/BlockMapAFUBase.hpp>

#include <BDPIDevice/BitPacking/Packer.hpp>
#include <BDPIDevice/BitPacking/Unpacker.hpp>
#include <BDPIDevice/BitPacking/Types/FixedInt.hpp>

#include <array>
#include <vector>

#include <boost/align/aligned_allocator.hpp>
#include <boost/align/is_aligned.hpp>

#include <boost/range.hpp>
#include <boost/range/algorithm.hpp>

#include <boost/random/uniform_int_distribution.hpp>
#include <boost/random/mersenne_twister.hpp>

#include <boost/multiprecision/cpp_int.hpp>

#include "MemItem.hpp"

#include <signal.h>

using namespace std;

volatile bool sigIntReceived=false;

void sigInterruptHandler(int sig)
{
	cout << "SIGINT received: "<< sig << endl;
	sigIntReceived=true;
}

//ostream& operator<<(ostream& os,const array<uint64_t,7>& a)

//typedef boost::multiprecision::number<boost::multiprecision::backends::cpp_int_backend<440,440,boost::multiprecision::unsigned_magnitude,boost::multiprecision::checked,void>>
//		UInt440;

typedef MemItem<
		FixedInt<uint16_t,15>,
		array<FixedInt<uint64_t,64>,7>> Item;

typedef array<uint64_t,8> Storage;

const size_t ALIGNMENT=128;
const size_t N=1024;
const size_t HEXDIGITS=7*64/4;

int main(/*int argc,char **argv*/)
{
	boost::random::mt19937_64 							rng;
	boost::random::uniform_int_distribution<unsigned> 	randomIndex;

	Storage zero;
	boost::fill(zero,0);

	vector<Storage,boost::alignment::aligned_allocator<Storage,ALIGNMENT>> input(2*N,zero),output(N,zero);
	vector<Item>	expected;
	map<uint64_t,array<FixedInt<uint64_t,64>,7>> contents;

	cout << hex << setfill('0');

	cout << "Generating stimulus" << endl;


	// generate writes of random data to 0..(N-1), updating memory contents
	for(unsigned i=0;i<N;++i)
	{
		Item item;

		boost::generate(item.data, ref(rng));

		item.addr=i;
		item.write=true;
		item.request=true;

		cout << "[" << setw(4) << hex << i << "] ";
		for(const auto el : item.data)
			cout << setw(16) << el;
		cout << endl;

		// update memory contents
		contents[item.addr.value()] = item.data;

		Packer<array<uint64_t,8>> P(448+15+2,input[i]);
		P & item;
	}

	cout << "Generating readback requests 0..(N-1)" << endl;

	for(unsigned i=0;i<N;++i)
	{
		Item item;
		item.addr=N-i-1;
		item.write=false;
		item.request=true;
		boost::fill(item.data,0);

		Item response;
		response.addr=0;
		response.write=false;
		response.request=false;
		response.data=contents.at(item.addr.value());

		expected.push_back(response);

		Packer<array<uint64_t,8>> P(448+15+2,input[N+i]);
		P & item;
	}

	BlockMapAFUBase afu("/dev/cxl/afu0.0d");

	cout << "Starting" << endl;

	afu.source(input.data(),2*N*sizeof(Storage));
	afu.destination(output.data(),N*sizeof(Storage));

	afu.start();

	cout << "Waiting for ready status" << endl;

	afu.awaitReady();
//	while(afu.mmio_read64(0)&0xff != 1)
//	{
//		cout << '.' << flush;
//		usleep(1000000);
//	}

	cout << "  Start kick" << endl;

	afu.mmio_write64(0,0x0ULL);		// start signal: write 0 to MMIO 0

	struct sigaction newaction, oldaction;

	// get existing signal mask, store it in newaction.sa_mask
	sigprocmask(SIG_SETMASK,nullptr,&newaction.sa_mask);

	newaction.sa_handler=sigInterruptHandler;
	//newaction.sa_sigaction=sigInterruptHandler;
	newaction.sa_flags=SA_SIGINFO;
	//newaction.sa_restorer=nullptr;

	sigaction(SIGINT,&newaction,&oldaction);

	while(afu.status() != BlockMapAFUBase::Done && !sigIntReceived)
	{
		cout << '.' << flush;
		usleep(1000000);
	}

	cout << "Done" << endl;

	unsigned requestCtr=0,responseCtr=0;
	size_t Nreq=2*N;

	for(requestCtr=responseCtr=0; requestCtr < Nreq; ++requestCtr)
	{
		Unpacker<array<uint64_t,8>> U(448+15+2,input[requestCtr]);
		Item req;

		U & req;

		if (req.request && req.write)
			cout << "Request " << setw(4) << requestCtr << " is a write - no response expected" << endl;
		else
		{
			Unpacker<array<uint64_t,8>> Uresp(448+15+2,output[responseCtr]);
			Item resp;

			Uresp & resp;

			if (!req.request)
			{
				cout << "Request " << setw(4) << requestCtr << " is a passthrough - expecting same data response [" << responseCtr << "]" << endl;

			}
			else
			{
				cout << "Request " << setw(4) << requestCtr << " is a read from address " << req.addr << " expected response [" << responseCtr << "]" << endl;
				cout << "  Expected data: ";

				for(unsigned j=0;j<7;++j)
					cout << setw(8) << expected[responseCtr].data[j].value();
				cout << endl;

				cout << "  Received data: ";
				for(unsigned j=0;j<7;++j)
					 cout << setw(8) << resp.data[j].value();
				cout << endl;

				bool equal=true;
				for(unsigned j=0;j<7;++j)
					equal &= expected[responseCtr].data[j].value() == resp.data[j].value();

				if (equal)
					cout << "    Match OK" << endl;
				else
					cout << "    *** ERROR ***" << endl;
//				cout << "  Expecting " << setw(HEXDIGITS) << req.data << endl;
//				cout << "  Received  " << setw(HEXDIGITS) << << endl;
			}
			++responseCtr;
		}
	}
}
