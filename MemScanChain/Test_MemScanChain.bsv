package Test_MemScanChain;

import GetPut::*;
import MemScanChain::*;
import AlteraM20k::*;
import BRAMStall::*;

import ClientServer::*;

import StmtFSM::*;
import Vector::*;

import FIFOF::*;

import Assert::*;
import PAClib::*;

import BDPIDevice::*;
import BDPIPort::*;

import BRAM::*;
import SpecialFIFOs::*;

import "BDPI" function Action bdpi_initMemScanChainTest;

function PipeOut#(t) gatePipeOut(Bool gate,PipeOut#(t) pi) = interface PipeOut;
    method Action deq if (gate) = pi.deq;
    method t first if (gate) = pi.first;
    method Bool notEmpty = pi.notEmpty;
endinterface;





/** Testbench for MemScanChainElement using SW (C++) testing
 *
 * Test a single element.
 *
 * Address width of the underlying BRAM specifies the depth of each element (here 2**8).
 * Element number 
 *
 * Address bits (x=Don't care, B=bank, O=offset)
 *       xxxxxxxBOOOOOOOO
 *
 *       1      87      0
 *       5
 *
 * Bank/offset splitting method is specified by the first argument to mkMemScanChainElement, here addressToLocalOffsetBankMSB
 *
 * Offset width is set by BRAM address width
 * Bank width is set by highest active bank number (here 1)
 * Total address width must be >= offset width + bank width
 */

module mkTB_MemScanChainElement_SW()
    provisos (
        NumAlias#(naddr,16),
        NumAlias#(ndata,32),
        Alias#(addrT,UInt#(naddr)),
        Alias#(dataT,Bit#(ndata))
    );

    // instantiate a MemScanChainTest C++ testbench, with stimulus and output ports
    BDPIDevice dev <- mkBDPIDevice(
        bdpi_initMemScanChainTest,
        bdpi_createDeviceFromFactory(
            "MemScanChainTest",         // the testbench object factory to use
            "file=input.txt",           // options to testbench factory
            32'h010001ff),              // address range { 16'lo, 16'hi }
        True);          // auto-init

    BDPIPortPipe#(MemItem#(naddr,dataT))  stim <- mkBDPIPortPipe(dev,0,constFn(noAction));
    BDPIPort#(void,MemItem#(naddr,dataT)) out  <- mkBDPIPort(dev,1,constFn(noAction));


    // Instantiate the backing store (BRAM) with address size 8 bits -> 2^8=256 elements per bank
    BRAM2PortStall#(UInt#(8), dataT) br <- mkBluespecBRAM2WithStall(256);


    function Action showStim(MemItem#(naddr,dataT) i) = $display($time," Input: ",fshow(i));
    let stimT <- mkTap(showStim,stim.pipe);


    // The DUT: a single element designated as bank 1
    PipeOut#(MemItem#(naddr,dataT)) dut <- mkMemScanChainElement(
        addressToLocalOffsetBankMSB(1),                             // arg is bank number, func splits as { pad, bank, offset }
        br.porta,stimT);

    Stmt master = seq
        await(stim.done);
        repeat(10) noAction;
        dev.close;
        $display($time,"That's all folks!");
        $finish;
    endseq;

    // sink output back to testbench
    mkSink_to_fa(out.write, dut);

    mkAutoFSM(master);
endmodule





/** Similar testbench to single element above, except produces a full scan-chain with 4 banks of 128 elements.
 *
 */

module mkTB_MemScanChain_SW()
    provisos (
        NumAlias#(nBRAM,4),
        NumAlias#(naddr,16),
        NumAlias#(bramDepth,128),
        Log#(bramDepth,bramAddrBits),
        NumAlias#(ndata,32),
        Alias#(addrT,UInt#(naddr)),
        Alias#(offsT,UInt#(bramAddrBits)),
        Alias#(dataT,Bit#(ndata))
    );

    BDPIDevice dev <- mkBDPIDevice(
        bdpi_initMemScanChainTest,
        bdpi_createDeviceFromFactory(
            "MemScanChainTest",
            "file=input.txt",
            32'h00000400),
        True);

    BDPIPortPipe#(MemItem#(naddr,dataT))  stim <- mkBDPIPortPipe(dev,0,constFn(noAction));
    BDPIPort#(void,MemItem#(naddr,dataT)) out  <- mkBDPIPort(dev,1,constFn(noAction));

    function Action showStim(MemItem#(naddr,dataT) i) = $display($time," Input: ",fshow(i));
    let stimT <- mkTap(showStim,stim.pipe);

    Vector#(nBRAM,BRAM2PortStall#(offsT,dataT)) br <- replicateM(mkBluespecBRAM2WithStall(valueOf(bramDepth)));

    PipeOut#(MemItem#(naddr,dataT)) chain <- mkMemScanChain(map(portA,br),stimT);

    Stmt master = seq
        await(stim.done);

        // give it a while to propagate through the chain
        repeat(100) noAction;
        dev.close;
        $display($time,"That's all folks!");
        $finish;
    endseq;

    // sink output back to testbench

    mkSink_to_fa(out.write, chain);

    mkAutoFSM(master);
endmodule





/** Uses a somewhat goofy argument structure to pass in some type parameters. Values of b,o don't matter; the first argument's
 * bit width is the number of banks and the second is the number of bits in the offset (nbOffs).
 *
 * Each of the (nBanks) banks is 2**(nbOffs) deep for a total depth of nBanks * 2^nbOffs.
 */

module mkSyn_MemScanChain#(Integer depth,UInt#(nBanks) b,UInt#(nbOffs) o)(Server#(MemItem#(nbAddr,dataT),MemItem#(nbAddr,dataT)))
    provisos (
        Alias#(UInt#(nbOffs),offsT),
        Alias#(UInt#(nbAddr),addrT),
        Add#(1,nonzero,nBanks),
        Log#(nBanks,nbBankIdx),
        Add#(nbOffs,nbBankIdx,nbMinAddr),
        Add#(nbMinAddr,__some,nbAddr),
        Add#(nbOffs,__more,nbAddr),
        Bits#(dataT,nbData)
    );

    Integer bankDepth = 2**valueOf(nbOffs);

    staticAssert(depth == bankDepth * valueOf(nBanks),"nBanks * bankDepth != depth");


    Vector#(nBanks,BRAM2PortStall#(UInt#(nbOffs),dataT)) br <- replicateM(mkBRAM2Stall(bankDepth));

    FIFOF#(MemItem#(nbAddr,dataT)) iFifo <- mkFIFOF, oFifo <- mkFIFOF;

    let chain <- mkMemScanChain(map(portA,br),f_FIFOF_to_PipeOut(iFifo));
    mkSink_to_fa(oFifo.enq, chain);

    interface Put request = toPut(iFifo);
    interface Get response = toGet(oFifo);
endmodule


// approximately the size of a 64k * 64b 8-bank accumulator (8 banks * 8k elements * 64b)
function module#(Server#(MemItem#(16,UInt#(64)),MemItem#(16,UInt#(64)))) mkSyn_MemScanChain_8_8k_64 = 
    mkSyn_MemScanChain(
        65536,      // 64k elements
        8'h0,       // 8 banks
        13'h0       // 13b offset (-> 3b bank (8 banks), 2**13=8k elements each)
        );

// approximately the size of a 32k x 440b geometry ram with 8 banks (8 banks * 4k elements * 440b)
function module#(Server#(MemItem#(15,Bit#(440)),MemItem#(15,Bit#(440)))) mkSyn_MemScanChain_8_4k_440 = 
    mkSyn_MemScanChain(
        32768,      // 32k elements
        8'h0,       // 8 banks
        12'h0       // 12b offset (-> 3b bank (8 banks), 2**12=4k elements each)
        );

function module#(Server#(MemItem#(15,Bit#(512)),MemItem#(15,Bit#(512)))) mkSyn_MemScanChain_2k_16_512 =
    mkSyn_MemScanChain(32768,16'h0,11'h0);

function module#(Server#(MemItem#(16,Bit#(512)),MemItem#(16,Bit#(512)))) mkSyn_MemScanChain_4k_16_512 =
    mkSyn_MemScanChain(65536,16'h0,12'h0);

function module#(Server#(MemItem#(16,Bit#(512)),MemItem#(16,Bit#(512)))) mkSyn_MemScanChain_8k_8_512 =
    mkSyn_MemScanChain(65536,8'h0,13'h0);

endpackage
