package BRAMGroup;

import BRAMStall::*;
import PAClib::*;
import Vector::*;

/**
 * In Bluespec packing convention, LSB/rightmost ends up in x[0], ie. 64b 0x0123456789ABCDEF split into 4 16b words
 *                                            [0]       [1]       [2]       [3]
 *                              words:        0xCDEF    0x89AB    0x4567    0x0123
 *
 * In a little-endian machine with ascending memory addresses ------>
 *                              bytes:        0xEF 0xCD 0xAB 0x89 0x67 0x45 0x23 0x01 
 *                              16b words:    0xCDEF    0x89AB    0x4567    0x0123
 *                              32b words:    0x89ABCDEF          0x01234567
 *                              64b word:     0x0123456789ABCDEF
 *
 * CAPI transfers cache lines as two 512b big-endian words.
 * Reading uint8_t* p yields p[0] in the MSB, p[63] in LSB when bwad=0, p[64] in MSB and p[127] in LSB of bwad=1.
 *
 * When working with n words per half-line, it makes sense to do an endian swap at the half-line level (before input to this module)
 * so that arrays are in correct order (x[0] in Vector#(n,t) <-> low-address element) and host word interpretation (LE) is
 * accounted for.
 */




/** Present a Vector#(n,BRAMPortStall#(addrT,dataT) as a single BRAMPortStall#(addrT,Vector#(n,dataT)) by ganging together the
 * address and fanning the data out to the constituent blocks.
 *
 * The same address is passed to all blocks. They are striped together s.t. br[0] corresponds to readdata.first[0]
 *
 * Methods with implicit conditions (deq, first, clear) depend on all BRAMs being ready
 * notEmpty will show if any BRAM is not empty
 */

function BRAMPortStall#(addrT,Vector#(n,dataT)) groupBRAMPorts(Vector#(n,BRAMPortStall#(addrT,dataT)) br) = 
    interface BRAMPortStall;
        method Action putcmd(Bool wr,addrT addr,Vector#(n,dataT) data) = 
            zipWithM_(
                doPutCmd(wr,addr),
                data,
                br);

        method Action clear = mapM_(doClear,br);

        interface PipeOut readdata;
            method Action deq = mapM_(doDeq,br);
            method Vector#(n,dataT) first = map(getFirst,br);
            method Bool notEmpty = Vector::\or (map(compose(getNotEmpty,getReadDataPipe),br));
        endinterface
    endinterface;




/** Switches between multiple BRAMs using the BRAM gate above.
 * Arguments are a selector signal and a single BRAM to be switched.
 * Output is a vector of BRAM ports, each corresponding to a value of select.
 *
 * o <- mkBRAMPortSwitch(n,sel,br);
 * 
 * o[sel]               gets to access the port
 * o[i]     i != sel    blocks putcmd/clear and readdata.deq; readdata.notEmpty and readdata.first still work as usual
 *
 */

function List#(BRAMPortStall#(addrT,dataT)) switchBRAMPort(Integer n,UInt#(ni) sel,BRAMPortStall#(addrT,dataT) br);
    // create the gated BRAM port for case sel==i
    // since conditions can be trivially proven to be mutually exclusive, shouldn't cause any conflict issues
    function brport(Integer i) = gateBRAMPort(fromInteger(i) == sel,br);

    return List::map(
            brport,        
            List::upto(0,n-1));
endfunction

endpackage
