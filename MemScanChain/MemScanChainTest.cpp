/*
 * MemScanChainTest.cpp
 *
 *  Created on: Apr 27, 2016
 *      Author: jcassidy
 */

#include <BDPIDevice/BitPacking/Types/std_bool.hpp>
#include <BDPIDevice/BitPacking/Types/std_tuple.hpp>

#include <BDPIDevice/DeviceFactory.hpp>

#include <vector>
#include <tuple>
#include <cinttypes>

#include <iostream>
#include <fstream>
#include <iomanip>

#include <boost/random/mersenne_twister.hpp>

#include <boost/multiprecision/gmp.hpp>

#include "MemScanChainTest.hpp"
#include "MemRequestFromFile.hpp"

using namespace std;

/// Register the factory so we can create MemScanChainTest by passing the string to bdpi_createDeviceFromFactory
StandardNewFactory<MemScanChainTest> MemScanChainTest::s_factory("MemScanChainTest");


// Set status to ready and have next request waiting
MemScanChainTest::StimulusPort::StimulusPort(MemScanChainTest* t,MemRequestGenerator* gen)
	: ReadPortPipeWrapper(t,50),
	  m_gen(gen)
{
	status(Wait);
}


void MemScanChainTest::StimulusPort::nextRequest()
{
	boost::optional<MemRequest> req = (*m_gen)();

	if (req)								// if valid request, set the output value
		set(m_current=*req);
	else if (m_gen->eof())
		status(End);
	else if (status() != Port::Wait)		// if in Wait status, output is already cleared
		clear();
}

/** Logs the stimulus provided to the m_history port
 *
 */

void MemScanChainTest::StimulusPort::deq()
{
	// log to the history vector
	device()->m_history.emplace_back(device()->timebase(),m_current);

	const auto f = std::cout.flags();
	cout << "Time " << setw(20) << dec << device()->timebase() << "  IN " << m_current << endl;
	cout.flags(f);

	if(m_current.request && device()->m_addrFirst <= m_current.addr.value() && m_current.addr.value() <= device()->m_addrLast)
	{
		if (m_current.write) 		// on write: update memory contents growing vector as needed
			device()->setContents(m_current.addr.value(), m_current.data.value());
		else
			device()->expectResponse(device()->getContents(m_current.addr));
	}
	else							// passthrough
		device()->expectRequest(m_current);

	device()->m_newRequestNextCycle = true;
}

void MemScanChainTest::setContents(Address addr,Data data)
{
	if (addr.value() >= m_contents.size())
		m_contents.resize(addr.value()+1,Data());
	m_contents[addr.value()] = data;
}

MemScanChainTest::Data MemScanChainTest::getContents(Address addr) const
{
	return m_contents.at(addr.value());
}

void MemScanChainTest::expectResponse(Data data)
{
	if (m_expect.full())
		throw std::out_of_range("MemScanChainTest::m_expect full");
	m_expect.push_back(make_pair(timebase(),MemRequest{false,0,false,data}));
}

void MemScanChainTest::expectRequest(MemRequest req)
{
	if (m_expect.full())
		throw std::out_of_range("MemScanChainTest::m_expect full");
	m_expect.push_back(make_pair(timebase(),req));
}

pair<uint64_t,MemScanChainTest::MemRequest> MemScanChainTest::expectedResponse()
{
	if(m_expect.empty())
	{
		cout << "ERROR: MemScanChainTest::m_expect empty" << endl;
		return make_pair(-1ULL, MemRequest());
	}

	pair<uint64_t,MemScanChainTest::MemRequest>	o = m_expect.front();
	m_expect.pop_front();
	return o;
}




/** Constructs the testbench. Argstr and data are passed to the makeGenerator factory.
 */

MemScanChainTest::MemScanChainTest(const char* argstr,const uint32_t* data) :
	Device({ &m_stimPort, &m_resultPort }),
	m_stimPort(this,makeGenerator(argstr,data)),
	m_addrFirst((*data & 0xffff0000) >> 16),
	m_addrLast(*data & 0x0000ffff),
	m_resultPort(this)
{
}



MemScanChainTest::~MemScanChainTest()
{
}


void MemScanChainTest::preClose()
{
	std::cout << "Testbench closing" << std::endl;
	std::cout << "  " << m_errCount << " errors" << std::endl;
}


MemScanChainTest::OutputPort::OutputPort(MemScanChainTest* t) : WritePortWrapper(t,50)
{
	status(Status::Ready);
}


void MemScanChainTest::OutputPort::write(const MemItem<MemScanChainTest::Address,MemScanChainTest::Data>& t)
{
	const auto f = cout.flags();
	cout << "Time " << setw(20) << dec << device()->timebase() << " OUT " << t << endl;

	pair<uint64_t,MemRequest> expected = device()->expectedResponse();

	if (expected.second == t)
	{
		//cout << "OK: " << expected.second << " from request at time " << expected.first << endl;
	}
	else
	{
		cout << "MISMATCH: Input at time " << expected.first << " returned " << t << " but expecting " << expected.second << endl;
		device()->m_errCount++;
	}

	cout.flags(f);
}

MemScanChainTest::MemRequestGenerator* MemScanChainTest::makeGenerator(const char *argstr,const uint32_t* data)
{
	cout << "Creating a memory request generator with argstr='" << argstr << "'" << endl;

	string arg(argstr);

	size_t s = arg.find_first_of("=");

	string type = arg.substr(0,s);
	string fn = arg.substr(s+1);

	cout << "type=" << type << " arg=" << fn << " s=" << s <<  endl;

	return new MemRequestFromFile(fn.c_str());
}

void MemScanChainTest::cycleStart()
{
	// automatically advance output if in wait state
	if (m_stimPort.status() == Port::Wait || m_newRequestNextCycle)
		m_stimPort.nextRequest();

	m_newRequestNextCycle=false;
}


extern "C" void bdpi_initMemScanChainTest(){}
