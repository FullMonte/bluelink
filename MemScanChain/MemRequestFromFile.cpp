/*
 * MemRequestFromFile.cpp
 *
 *  Created on: Mar 8, 2017
 *      Author: jcassidy
 */

#include "MemRequestFromFile.hpp"


using namespace std;

MemRequestFromFile::MemRequestFromFile(const char* fn) :
		m_filename(fn),
		m_is(fn)
{
	if (!m_is.good())
		throw std::logic_error("Failed to open file");
}

boost::optional<MemScanChainTest::MemRequest> MemRequestFromFile::operator()()
{
	char type;
	m_is >> type;

	MemScanChainTest::Data 		data;
	MemScanChainTest::Address 	addr;

	if (m_is.fail() || m_is.eof())
		return boost::optional<MemScanChainTest::MemRequest>();

	switch(type)
	{
	case 'W':
		m_is >> addr >> data;
		return MemScanChainTest::MemRequest{ true, addr, true, data };

	case 'R':
		m_is >> addr;
		return MemScanChainTest::MemRequest{ true, addr, false, 0 };

	case 'P':
		m_is >> data;
		return MemScanChainTest::MemRequest{ false, 0, false, data };

	case 'I':
		cout << "Idle" << endl;
		return boost::optional<MemScanChainTest::MemRequest>();
	}

	cout << "ERROR - Invalid request-type character '" << type << "' in MemRequestFromFile::operator()()" << endl;
	throw std::logic_error("Invalid input from stimulus file");
}

bool MemRequestFromFile::eof() const
{
	return m_is.eof();
}
