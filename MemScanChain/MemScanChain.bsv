package MemScanChain;

import Assert::*;

import AlteraM20k::*;
import BRAMStall::*;
import PAClib::*;
import FIFOF::*;
import GetPut::*;
import BuildVector::*;

import Vector::*;

//export mkMemScanChain,MemItem,MemRequest,MemRequestOperation;

/**** Memory scan chain
 *
 * Takes a vector of BRAMPortStall#(addrT,dataT) and links them together into a scan chain providing access to all elements.
 * 
 * At the top level, the chain accepts elements of type MemItem#(nbAddr,dataT), which can be either a request or a response.
 * Requests are passed through until hitting the matching element, which generates a response that is passed along.
 *
 * Responses should be generated in request order.
 *
 * The advantage of scan-chain linkage is that it eliminates the need to fan out requests and fan in responses from all N block
 * RAMs. Instead, each RAM is linked to its immediate neighbours only via FIFOs. Request latency is long but throughput is one
 * per cycle and timing should be met more easily.
 *
 *
 * Public Types
 *
 *  MemItem#(nbAddr,dataT)       holds either a request or response data
 *      MemRequest#(nbAddr,dataT)    holds a memory request specifying address and operation (read or write+data)
 *          MemRequestOperation#(dataT)  can be either read or a write+data
 *
 *
 * Public Modules
 * 
 *  mkMemScanChain              generates a scan chain from a vector of BRAMPortStall#(addrT,dataT) resulting in a
 *                                  Pipe#(MemItem#(addrT,dataT)) interface
 *
 *
 * Internal Modules
 *  
 *  mkMemScanChainElement       takes an input PipeOut#(MemItem#(nbAddr,dataT)), interfaces it to a provided BRAM, and generates
 *                                  output (passthrough/request). Instantiated by mkMemScanChain.
 *
 *
 *
 */



typedef union tagged {
    void    Read;
    dataT   Write;
} MemRequestOperation#(type dataT) deriving(Bits);

instance FShow#(MemRequestOperation#(dataT)) provisos (FShow#(dataT));
    function Fmt fshow(MemRequestOperation#(dataT) op) = case (op) matches
        tagged Read:
            return fshow("Read");
        tagged Write .data:
            return fshow("Write ") + fshow(data);
    endcase;

endinstance

typedef struct {
    UInt#(nbAddr)                   addr;
    MemRequestOperation#(dataT)     op;
} MemRequest#(numeric type nbAddr,type dataT) deriving(Bits);

instance FShow#(MemRequest#(nbAddr,dataT)) provisos (FShow#(dataT));
    function Fmt fshow(MemRequest#(nbAddr,dataT) req) = fshow("Addr ") + $format("%08X",req.addr) + fshow(req.op);
endinstance


/** The items that pass through the scan chain: starts as a request which passes along until it meets the matching requestee,
 * which replaces the request with a response. */

typedef union tagged {
    dataT                               Response;
    MemRequest#(nbAddr,dataT)           Request;
} MemItem#(numeric type nbAddr,type dataT) deriving(Bits);


instance FShow#(MemItem#(nbAddr,dataT)) provisos (FShow#(dataT));
    function Fmt fshow(MemItem#(nbAddr,dataT) i) = case (i) matches
        tagged Response .data:
            return fshow("Response ") + fshow(data);
        tagged Request .req:
            return fshow("Request  address ") + fshow(req.addr) + fshow(" ") + fshow(req.op);
    endcase;
endinstance



/** Interfaces a pipe-interfaced BRAM port (BRAMPortStall) into a scan chain.
 *
 * Type parameters
 *  nbOffs      Exact number of bits to express the offset component of the address (depth must be 2**noffs)
 *  nbAddr      Number of scan chain address bits
 *  nbBank      (implied from naddr-noffs) number of bits to address the bank
 * 
 */


/** Checks if the address addr = { bank, offs } belongs to the specified bank
 * If so, returns the offset within the bank
 */

function Maybe#(UInt#(nbOffs)) addressToLocalOffsetBankMSB(Integer bk,UInt#(nbAddr) addr)
    provisos (BitExtend#(nbOffs,nbAddr,UInt));
    UInt#(nbOffs) offs = truncate(addr);

    return (addr>>valueOf(nbOffs)) == fromInteger(bk) ? tagged Valid offs : tagged Invalid;
endfunction
    

module mkMemScanChainElement#(
        function Maybe#(UInt#(nbOffs)) addressToLocalOffset(UInt#(nbAddr) a),
            // returns Invalid if address is not local to this bank, or a Valid offset

        BRAMPortStall#(UInt#(nbOffs),dataT) brport,
            // BRAM port handling the local requests; RAM size must be 2**nbOffs

        PipeOut#(MemItem#(nbAddr,dataT)) pi)     // Incoming pipe
    (PipeOut#(MemItem#(nbAddr,dataT)))
    provisos (
        Add#(nbOffs,nbBank,nbAddr),
        Bits#(dataT,nbData)
    );

    // 2-stage input FIFO to ensure no combinational path going upstream
    FIFOF#(MemItem#(nbAddr,dataT))     scanChainInput  <- mkFIFOF;
    mkSink_to_fa(scanChainInput.enq,pi);



    function Maybe#(UInt#(nbOffs)) localOffset(MemItem#(nbAddr,dataT) item) = case(item) matches
        tagged Response .*:     tagged Invalid;
        tagged Request .req:    addressToLocalOffset(req.addr);
    endcase;


    // local requests absorb input and may generate output if it's a read
    rule acceptLocalRequest if (scanChainInput.first matches tagged Request .req
            &&& addressToLocalOffset(req.addr) matches tagged Valid .offs);
        scanChainInput.deq;

        case (req.op) matches
            tagged Read:            brport.putcmd(False,offs,?);
            tagged Write .data:     brport.putcmd(True,offs,data);
        endcase

    endrule



    // output buffer registers mux outputs

    FIFOF#(MemItem#(nbAddr,dataT)) oBuf <- mkLFIFOF;

    (* descending_urgency="outputReadData,outputPassthrough" *)

    rule outputReadData;
        brport.readdata.deq;
        oBuf.enq(tagged Response brport.readdata.first);
    endrule

    rule outputPassthrough if (!isValid(localOffset(scanChainInput.first)));
        scanChainInput.deq;
        oBuf.enq(scanChainInput.first);
    endrule

    return f_FIFOF_to_PipeOut(oBuf);
endmodule




/** Creates a memory scan chain of MemScanChainElement as defined above, from an array of block RAM ports.
 *
 * The result is a simple Pipe#(MemItem#(addrT,dataT)), enabling concatenation of multiple scan chains.
 *
 *  addrT,dataT     Scan chain address and data
 *  UInt#(nboffs)   Element indexing (element depth must be exactly 2**noffs)
 */

module mkMemScanChain#(
    Vector#(nBanks,BRAMPortStall#(UInt#(nbOffs),dataT)) brport,
    PipeOut#(MemItem#(nbAddr,dataT)) pi)
    (PipeOut#(MemItem#(nbAddr,dataT)))
    provisos (
        Log#(nBanks,nbBank),
        BitExtend#(nbOffs,nbAddr,UInt),

        Add#(nbOffs,nbBank,nbMinAddress),    // ensure address line is big enough for contents

        Add#(1,__lessthan2,nBanks),
        Bits#(dataT,nbData)
    );

    // The scan chain elements
    Vector#(nBanks,PipeOut#(MemItem#(nbAddr,dataT))) el;

    el[0]  <- mkMemScanChainElement(addressToLocalOffsetBankMSB(0), brport[0],pi);

    for(Integer i=1;i<valueOf(nBanks);i=i+1)
        el[i] <- mkMemScanChainElement(addressToLocalOffsetBankMSB(i), brport[i], el[i-1]);

    return last(el);
endmodule



endpackage
