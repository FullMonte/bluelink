package Test_MemScanChain_CAPI;

import AlteraM20k::*;
import MMIO::*;
import BlockMapAFU::*;

import Test_MemScanChain::*;
import MemScanChain::*;
import Vector::*;
import BRAMStall::*;

import AFU::*;
import AFUHardware::*;
import AFUShims::*;
import DedicatedAFU::*;
import Endianness::*;

import CreditIfc::*;

import PAClib::*;

import FIFOF::*;

import CAPIOptions::*;
import SynthesisOptions::*;

/** Streams requests in and responses out from a MemScanChain.
 */

module [ModuleContext#(ctxT)] mkMemScanChainBlockMapAdapter(BlockMapAFU#(Bit#(512),Bit#(512)))
    provisos (
        NumAlias#(nBanks,8),
        Gettable#(ctxT,SynthesisOptions));
    FIFOF#(Bit#(64)) mmResp <- mkFIFOF1;

    Integer bankDepth=4096;

    ctxT ctx <- getContext;
    SynthesisOptions opts = getIt(ctx);

    // conversion to/from raw bits (15 address + 2 flags + 448 data -> 512b padded) on host side
    function MemItem#(15,Bit#(448))     unpad(Bit#(512) i) = unpack(truncate(endianSwap(i)));
    function Bit#(512)                  pad(MemItem#(15,Bit#(448)) v) = endianSwap(extend(pack(v)));

    FIFOF#(void) stDone <- mkGFIFOF1(True,True);

    Bool showCredits=False;
    Integer creditDepth = 128;

    CreditManager#(UInt#(9)) credits <- mkCreditManager(CreditConfig {
        bypass: False,
        initCredits: creditDepth-2,
        maxCredits: creditDepth-2 });

    FIFOF#(Bit#(512)) iFifo <- mkLFIFOF;

    // strip padding
    let stim <- mkFn_to_Pipe(unpad,f_FIFOF_to_PipeOut(iFifo));

    function Action takeCredit(sometype i) = action
        if (showCredits)
            $display($time," Taking a credit (precount=%d)",credits.count);
        credits.take;
    endaction;

    function Action showInput(t i) provisos (FShow#(t)) = $display($time," INPUT:  ",fshow(i));

    function Action showOutput(t o) provisos (FShow#(t)) = $display($time," OUTPUT: ",fshow(o));

    rule showStall if (credits.count == 0 && showCredits);
        $display($time," Stalled for lack of credits");
    endrule

    let stimT <- mkTap(takeCredit,stim);
    let stimTA <- mkTap(showInput,stim);

    Vector#(nBanks,BRAM2PortStall#(UInt#(12),Bit#(448))) br;

    
    if (opts.mem matches tagged AlteraStratixV)
        br <- replicateM(mkBRAM2Stall(bankDepth));
    else
        br <- replicateM(mkBluespecBRAM2WithStall(bankDepth));

    let dut <- mkMemScanChain(map(portA,br),stimTA);

    let dutT <- mkTap(showOutput,dut);

    let obuf <- mkBuffer_n(creditDepth,dutT);

    function Action giveCredit(othertype i) = action
        if (showCredits)
            $display($time," Returning a credit (precount=%d)",credits.count);
        credits.give;
    endaction;

    let obufT <- mkTap(giveCredit,obuf);

    let o <- mkFn_to_Pipe(pad,obufT);

    interface Server stream;
        interface Put request = toPut(iFifo);
        interface Get response = toGet(o);
    endinterface

    /** No MMIO interface */

    interface Server mmio;
        interface Put request;
            method Action put(MMIORWRequest req);
                mmResp.enq(64'h0); 
            endmethod
        endinterface

        interface Get response;
            method ActionValue#(Bit#(64)) get;
                mmResp.deq;
                return mmResp.first;
            endmethod
        endinterface
    endinterface

    method Action istreamDone = noAction;
    method Action ostreamDone = stDone.enq(?);

    method Action rst  = stDone.deq;
    method Bool done = stDone.notEmpty;

endmodule


/** Base wrapper with context provided
 */

(*clock_prefix="ha_pclock"*)
module [ModuleContext#(ctxT)] mkMemScanChainCAPIBase(AFUHardware#(2))
    provisos (
        Gettable#(ctxT,SynthesisOptions),
        Gettable#(ctxT,CAPIOptions));

    let pt <- mkMemScanChainBlockMapAdapter;
    let dut <- mkBlockMapAFU(32,32,pt);
    let afu <- mkDedicatedAFU(dut,0);

    AFUHardware#(2) hw <- mkCAPIHardwareWrapper(afuParityWrapper(afu));
    return hw;
endmodule



/** Specific instance for hardware synthesis
 */

(*clock_prefix="ha_pclock"*)
module [Module] mkMemScanChainCAPI(AFUHardware#(2));
    SynthesisOptions syn = defaultValue;
    CAPIOptions capi = defaultValue;

    capi.showStatus = True;
    capi.showData = True;

    let { ctx, _w } <- runWithContext(
        hCons(syn,hCons(
        capi,hNil)),
        mkMemScanChainCAPIBase);
    return _w;
endmodule

endpackage
