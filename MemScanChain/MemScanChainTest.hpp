/*
 * MemScanChainTest.hpp
 *
 *  Created on: Mar 8, 2017
 *      Author: jcassidy
 */

#ifndef MEMSCANCHAIN_MEMSCANCHAINTEST_HPP_
#define MEMSCANCHAIN_MEMSCANCHAINTEST_HPP_

#include <BDPIDevice/Device.hpp>
#include <BDPIDevice/ReadPortPipeWrapper.hpp>
#include <BDPIDevice/WritePortWrapper.hpp>

#include <boost/circular_buffer.hpp>

#include <BDPIDevice/StandardNewFactory.hpp>

#include <BDPIDevice/BitPacking/Types/FixedInt.hpp>

#include "MemItem.hpp"

#include <type_traits>

#include <boost/optional.hpp>


/** BDPIDevice derived class for testing a MemScanChain using 16b addresses and 32b data.
 *
 */


class MemScanChainTest : public Device
{
public:
	typedef FixedInt<uint16_t,16>	Address;
	typedef FixedInt<uint32_t,32>	Data;
	typedef MemItem<Address,Data>	MemRequest;

	/** Base class for anything that can generate a memory request */
	class MemRequestGenerator
	{
	public:
		typedef MemItem<Address,Data>			RequestType;

		/// Valid if there is input present; default-init indicates no data available
		virtual boost::optional<RequestType>	operator()()=0;

		/// True if end of stream
		virtual bool eof() 		const { return false; 	}
	};

private:

	/** Stimulus port provides a PipeOut with read/write/passthrough inputs */

	class StimulusPort : public ReadPortPipeWrapper<
		MemScanChainTest::StimulusPort,
		MemScanChainTest,
		MemRequest>
	{
	public:
		StimulusPort(MemScanChainTest* t,MemRequestGenerator* gen);
		void nextRequest();
		void deq();

	private:
		MemItem<Address,Data>		m_current;
		MemRequestGenerator*		m_gen=nullptr;
	};

	class OutputPort : public WritePortWrapper<
		OutputPort,
		MemScanChainTest,
		MemItem<Address,Data>>
	{
	public:
		OutputPort(MemScanChainTest* t);
		void write(const MemItem<Address,Data>& t);
	};




public:
	MemScanChainTest(const char* argstr,const uint32_t* data);
	virtual ~MemScanChainTest();

private:

	void 	setContents(Address addr,Data data);
	Data	getContents(Address) const;

	void	expectRequest(MemRequest req);
	void	expectResponse(Data data);

	std::pair<uint64_t,MemRequest> expectedResponse();


	MemRequestGenerator* makeGenerator(const char* argstr,const uint32_t* data);

	static StandardNewFactory<MemScanChainTest> s_factory;

	virtual void cycleStart() override;
	virtual void preClose() override;

	StimulusPort						m_stimPort;
	OutputPort							m_resultPort;

	std::vector<std::pair<uint64_t,MemItem<Address,Data>>>	m_history;		// history of values deq'd, with timestamp
	std::vector<Data>										m_contents;		// memory contents

	// expected responses, timestamped with the time the request was deq'd
	boost::circular_buffer<std::pair<uint64_t,MemItem<Address,Data>>>	m_expect{100};

	uint64_t			m_addrFirst=0x0000000000000100ULL;
	uint64_t			m_addrLast =0x00000000000001FFULL;

	bool m_newRequestNextCycle=false;
	unsigned m_errCount=0;
};




#endif /* MEMSCANCHAIN_MEMSCANCHAINTEST_HPP_ */
