package Test_BRAMGroup;

import BRAMStall::*;
import AlteraM20k::*;
import StmtFSM::*;
import Vector::*;
import BuildVector::*;
import PAClib::*;
import BRAMGroup::*;

import Assert::*;

/** Tests the BRAM grouping using very simple stimulus

 * 8-bit address, 32-bit data, x4 grouping
 *
 * Physical view: 4 256x32b blocks      with 8-bit address, 32-bit data
 * Logical view:  256x128b single block with 8-bit address, 128-bit data
 */

module mkTB_BRAMGroup()
    provisos (
        NumAlias#(nBlocks,4),
        NumAlias#(depth,256),

        NumAlias#(nbAddr,8),
        Alias#(addrT,UInt#(nbAddr)),

        NumAlias#(nbData,32),
        Alias#(dataT,UInt#(nbData)),

        Mul#(nbData,nBlocks,nbGroupData),
        Alias#(groupDataT,Bit#(nbGroupData))
        );

    // enable-reg for group access (false -> individual access enabled)
    Reg#(Bool) groupEn <- mkReg(True);

    // create the array and form a vector of the port A's
//    Vector#(nBlocks,BRAM2PortStall#(addrT,dataT)) br <- replicateM(mkBRAM2Stall(valueOf(depth)));
    Vector#(nBlocks,BRAM2PortStall#(addrT,dataT)) br <- replicateM(mkBluespecBRAM2WithStall(valueOf(depth)));

    let g = groupBRAMPorts(map(portA,br));

    let brIndividual = map(gateBRAMPort(!groupEn),map(portA,br));
    let brGroup      = gateBRAMPort(groupEn,g);

    // Continuously peek at individual and group output, maintaining RWire/vector of RWire as appropriate
    Vector#(nBlocks,RWire#(dataT)) individualDataOut <- replicateM(mkRWire);

    for(Integer i=0;i<valueOf(nBlocks);i=i+1)
        rule checkIndividualOutput;
            individualDataOut[i].wset(brIndividual[i].readdata.first);
        endrule

    RWire#(Vector#(nBlocks,dataT)) groupDataOut <- mkRWire;
    
    rule checkGroupOutput;
        groupDataOut.wset(brGroup.readdata.first);
    endrule


    // functions to implement group & individual read/write/deq with display
    function Action groupRead(addrT addr) = action
        brGroup.putcmd(False,addr,?);
        $display($time," Group read  address %04X",addr);
    endaction;

    function Action groupWrite(addrT addr,groupDataT data) = action
        brGroup.putcmd(True,addr,unpack(data));
        $display($time," Group write address %04X data %032X",addr,data);
    endaction;

    function Action groupDeq = action
        brGroup.readdata.deq;
        $display($time," Group deq data %x",brGroup.readdata.first);
    endaction;

    function Action iRead(Integer i,addrT addr) = action
        brIndividual[i].putcmd(False,addr,?);
        $display($time," Individual read block %02X address %08X",i,addr);
    endaction;

    function Action iWrite(Integer i,addrT addr,dataT data) = action
        brIndividual[i].putcmd(True,addr,data);
        $display($time," Individual write block %02X address %08X data %032X",i,addr,data);
    endaction;

    function Action iDeq(Integer i) = action
        brIndividual[i].readdata.deq;
        $display($time," Individual block %02X deq",i);
    endaction;

    function Action requireIndividualData(Integer i,dataT d) = action
        $display($time, " Block %2x returned data %x",i,d);
        dynamicAssert(isValid(individualDataOut[i].wget),"Missing data");
        dynamicAssert(individualDataOut[i].wget.Valid == d,"Data mismatch");
    endaction;

    function Action requireNoIndividualData(Integer i) = 
        dynamicAssert(!isValid(individualDataOut[i].wget),"Unexpected data on port ");

    Stmt stim = seq
        groupWrite(0,128'hffff0003ffff0002ffff0001ffff0000);
        groupRead(0);
        groupDeq;
        noAction;
//        par
//            dynamicAssert(brGroup.readdata.notEmpty,"Missing expected group readdata");
//            dynamicAssert(brGroup.readdata.first == 128'hffff0003ffff0002ffff0001ffff0000,"Data is not as expected");
//            groupDeq;
//        endpar


        // do simultaneous individual reads, checking that striping is reasonable

        groupEn <= False;

        action
            for(Integer i=0;i<valueOf(nBlocks);i=i+1)
                iRead(i,0);
        endaction
        noAction;

        // require read response with correct data
        action
            requireIndividualData(0,32'hffff0000);
            requireIndividualData(1,32'hffff0001);
            requireIndividualData(2,32'hffff0002);
            requireIndividualData(3,32'hffff0003);
        endaction

        // require data to be held        
        action
            requireIndividualData(0,32'hffff0000);
            requireIndividualData(1,32'hffff0001);
            requireIndividualData(2,32'hffff0002);
            requireIndividualData(3,32'hffff0003);
            for(Integer i=0;i<valueOf(nBlocks)-1;i=i+1)
                iDeq(i);
        endaction
        
        action
            requireNoIndividualData(0);
            requireNoIndividualData(1);
            requireNoIndividualData(2);
            requireIndividualData(valueOf(nBlocks)-1,32'hffff0003);
            iDeq(valueOf(nBlocks)-1);
        endaction

        action
            for(Integer i=0;i<valueOf(nBlocks);i=i+1)
                requireNoIndividualData(i);
        endaction


        // a series of individual writes, with leading 2 bytes going aaaa, bbbb, cccc, dddd
        // and trailing 2 bytes going 0,1,2,3

        action
            for(Integer i=0;i<valueOf(nBlocks);i=i+1)
                iWrite(i,1,32'haaaa0000 | fromInteger(i));
        endaction
        action
            for(Integer i=0;i<valueOf(nBlocks);i=i+1)
                iWrite(i,2,32'hbbbb0000 | fromInteger(i));
        endaction
        action
            for(Integer i=0;i<valueOf(nBlocks);i=i+1)
                iWrite(i,3,32'hcccc0000 | fromInteger(i));
        endaction
        action
            for(Integer i=0;i<valueOf(nBlocks);i=i+1)
                iWrite(i,4,32'hdddd0000 | fromInteger(i));
        endaction

        groupEn <= True;

        groupRead(0);
        groupRead(1);

        groupDeq;

        action
            groupRead(2);
            groupDeq;
        endaction

        delay(2);

        groupDeq;

        groupRead(3);

        action
            groupRead(4);
            groupDeq;
        endaction

        groupDeq;

        repeat(10) noAction;
    endseq;

    mkAutoFSM(stim);
endmodule

endpackage
