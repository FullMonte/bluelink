/*
 * MemItem.hpp
 *
 *  Created on: Mar 8, 2017
 *      Author: jcassidy
 */

#ifndef MEMSCANCHAIN_MEMITEM_HPP_
#define MEMSCANCHAIN_MEMITEM_HPP_

/** The request/response type circulated through the scan chain
 *
 * Request=true, addr, write=false 			-> read request
 * Request=true, addr, write=true, data		-> write request
 * Request=false, data						-> response data
 */

template<typename AddressType,typename DataType>struct MemItem
{
	typedef AddressType Address;
	typedef DataType	Data;

	bool 		request=false;
	Address 	addr;
	bool 		write=false;
	Data 		data;

	static constexpr struct Read_tag {} Read = Read_tag{};
	static constexpr struct Write_tag {} Write = Write_tag{};
	static constexpr struct Passthrough_tag {} Passthrough = Passthrough_tag{};

	MemItem(){}
	MemItem(bool request_,Address addr_,bool write_,Data data_) :
		request(request_),addr(addr_),write(write_),data(data_){}

	MemItem(const Read_tag,Address addr_) : request(true), addr(addr_){}
	MemItem(const Write_tag,Address addr_,Data data_) : request(true), addr(addr_), write(true), data(data_){}
	MemItem(const Passthrough_tag,Data data_) : data(data_){}

	template<class Archive>void serialize(Archive& ar,const unsigned)
		{ ar & request & addr & write & data; }
};

template<typename AddressType,typename DataType>bool operator==(const MemItem<AddressType,DataType>& lhs,const MemItem<AddressType,DataType>& rhs)
{
	if (lhs.request)
		return rhs.request &&					// rhs must also be request
				lhs.addr == rhs.addr &&			// with same type
				lhs.write == rhs.write && 		// and same address
				(!lhs.write || lhs.data == rhs.data);	// same data if a write;
	else
		return !rhs.request &&					// rhs must also not be a request
				lhs.data == rhs.data;			// and data must match
}



#include <iostream>
#include <iomanip>

template<class AddressType,class DataType>std::ostream& operator<<(std::ostream& os,const MemItem<AddressType,DataType>& t)
{
	if (!t.request)
	{
		os << "Data:    " << t.data;
	}
	else
	{
		os << "Request: " << (t.write ? "Write" : " Read") << " address " << t.addr;
		if (t.write)
			os << " data " << t.data;
	}
	return os;
}


#endif /* MEMSCANCHAIN_MEMITEM_HPP_ */
