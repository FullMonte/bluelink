package SynthesisOptions;

import HList::*;
import ModuleContext::*;
import DefaultValue::*;

export MemSynthesisOptions(..), DSPSynthesisOptions(..), SynthesisOptions(..), HList::*, ModuleContext::*, DefaultValue::*;

typedef union tagged
{
    void AlteraStratixV;
    void BSVBehavioral;
} MemSynthesisOptions;

typedef union tagged
{
    void AlteraStratixV;
    void BSVBehavioral;
} DSPSynthesisOptions;

typedef struct
{
    Bool disableCheckCode;              // disable display/assertions whose only intention is testing
    Bool showStatus;
    Bool showData;
    Bool verbose;
    Bool showPipeline;      
    Bool showIntersection;  
    Bool showBoundary;      
    Bool showRandqueue;     
    Bool showReflectRefract;
    Bool showStep;
    Bool showScatter;       
    Bool showStepFinish; 
    Bool showCache;
    Bool showEventCache;
    Bool showBRAM;    
    DSPSynthesisOptions dsp;
    MemSynthesisOptions mem;
} SynthesisOptions;

instance DefaultValue#(SynthesisOptions);
    function SynthesisOptions defaultValue = SynthesisOptions {
        disableCheckCode:   False,
        showStatus:         False,
        showData:           False,
        verbose:            False,
        showPipeline:       False,
        showIntersection:   False,  
        showBoundary:       False,      
        showRandqueue:      False,     
        showReflectRefract: False,
        showStep:           False,
        showScatter:        False,       
        showStepFinish:     False,
        showCache:          False,
        showEventCache:     False,
        showBRAM:           False,    

        dsp: AlteraStratixV,
        mem: AlteraStratixV
    };
endinstance

endpackage
