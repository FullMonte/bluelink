## 

mkdir -p Build/Release
cd Build/Release
cmake \
	-DBOOST_ROOT=/home/jcassidy/usr/boost_1_60_0 \
	-DCMAKE_MODULE_PATH=/home/jcassidy/remote-build/CAPICMake \
	-DCAPI_SYN_LIBCXL_DIR=/home/wagnerse/libcxl-1.3 \
	../..