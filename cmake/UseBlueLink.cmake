SET(BlueLink_LIBRARIES BlueLinkHost)

INCLUDE_DIRECTORIES(${BlueLink_INCLUDE_DIRS})
LINK_DIRECTORIES(${BlueLink_LIBRARY_DIRS})
LIST(APPEND Bluespec_BSC_PATH ${BlueLink_BDIR})

SET(BlueLink_HA_ASSIGNMENT_DELAY "#1" CACHE STRING "Assignment delay for host-to-AFU signals")

#ADD_CUSTOM_TARGET(SynthesisOptions DEPENDS ${BlueLink_BDIR}/SynthesisOptions.bo)
#ADD_CUSTOM_TARGET(BlockMapAFU DEPENDS ${BlueLink_BDIR}/BlockMapAFU.bo)

#INCLUDE(${BlueLink_DIR}/BlueLinkHost-export.cmake)

IF(VSIM_FOUND)
	VSIM_MAP_LIBRARY(bsvaltera ${BlueLink_VSIM_ALTERA_LIBRARY})
ENDIF()
